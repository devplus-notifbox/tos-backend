require("dotenv").config;

const config = {
  dialect: "mysql",
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  // schema: "public",
  dialectOptions: {
    timezone: "+07:00",
  },
  timezone: "+07:00",
};

module.exports = {
  development: config,
  test: config,
  production: config,
};
