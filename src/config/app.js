require("dotenv").config();

module.exports = {
  port: 3000,
  timeout: 60000,
  timezone: "Asia/Jakarta",
  whitelistUrl: process.env.WHITELIST_URL
    ? process.env.WHITELIST_URL.split(",")
    : [],
  apiRoot: "/api",
  defaultPath: process.env.DEFAULT_ROUTE || "/",
  url: process.env.BASE_URL,
  fileUrl: process.env.FILE_URL,
  maxFileSize: 1024 * 1024 * 3,
  allowedFileType: ["image/png", "image/jpeg", "application/pdf"],
};
