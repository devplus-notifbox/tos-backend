module.exports = {
  listJenisKendaraan: [
    { id: "mobil", nama: "Mobil Penumpang Umum" },
    { id: "bus-kecil", nama: "Bus Kecil" },
    { id: "bus-sedang", nama: "Bus Sedang" },
    { id: "bus-besar-tunggal", nama: "Bus Besar Lantai Tunggal" },
    { id: "bus-besar-ganda", nama: "Bus Besar Lantai Ganda" },
  ],
  listTipe: [
    { id: "akap", text: "AKAP" },
    { id: "akdp", text: "AKDP" },
    { id: "pariwisata", text: "Pariwisata" },
  ],
};
