const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getAction,
  dataAction,
  postAction,
  patchAction,
  deleteAction,
  getListAction,
} = require("../../../app/controllers/action");

/**
 * @swagger
 *
 * /action:
 *  get:
 *    tags:
 *      - "action"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Action ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/actionData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/action", callback(getAction));

/**
 * @swagger
 *
 * /action:
 *  post:
 *    tags:
 *      - "action"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Action Baru"
 *        schema:
 *          $ref: "#/components/schemas/actionData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/action", callback(postAction));

/**
 * @swagger
 *
 * /action:
 *  patch:
 *    tags:
 *      - "action"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Action ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Action"
 *        schema:
 *          $ref: "#/components/schemas/actionData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/action", callback(patchAction));

/**
 * @swagger
 *
 * /action:
 *  delete:
 *    tags:
 *      - "action"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Action ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/action", callback(deleteAction));

/**
 * @swagger
 *
 * /action/data:
 *  post:
 *    tags:
 *      - "action"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/action/data", callback(dataAction));

/**
 * @swagger
 *
 * /action/list:
 *  get:
 *    tags:
 *      - "action"
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: [{"id":"", "slug":"", "name":""}]
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/action/list", callback(getListAction));

module.exports = router;
