const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getModule,
  dataModule,
  postModule,
  patchModule,
  deleteModule,
  getListModule,
} = require("../../../app/controllers/module");

/**
 * @swagger
 *
 * /module:
 *  get:
 *    tags:
 *      - "module"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Module ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/moduleData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/module", callback(getModule));

/**
 * @swagger
 *
 * /module:
 *  post:
 *    tags:
 *      - "module"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Module Baru"
 *        schema:
 *          $ref: "#/components/schemas/moduleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/module", callback(postModule));

/**
 * @swagger
 *
 * /module:
 *  patch:
 *    tags:
 *      - "module"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Module ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Module"
 *        schema:
 *          $ref: "#/components/schemas/moduleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/module", callback(patchModule));

/**
 * @swagger
 *
 * /module:
 *  delete:
 *    tags:
 *      - "module"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Module ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/module", callback(deleteModule));

/**
 * @swagger
 *
 * /module/data:
 *  post:
 *    tags:
 *      - "module"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/module/data", callback(dataModule));

/**
 * @swagger
 *
 * /module/list:
 *  get:
 *    tags:
 *      - "module"
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: [{"id":"", "slug":"", "name":""}]
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/module/list", callback(getListModule));

module.exports = router;
