const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getOperasional,
  dataOperasional,
  postOperasional,
  patchOperasional,
  deleteOperasional,
  postUploadOperasional,
  dataPenumpangBerangkat,
} = require("../../../app/controllers/operasional");

/**
 * @swagger
 *
 * /operasional:
 *  get:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Operasional ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/operasionalData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/operasional", callback(getOperasional));

/**
 * @swagger
 *
 * /operasional:
 *  post:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Operasional Baru"
 *        schema:
 *          $ref: "#/components/schemas/operasionalData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/operasional", callback(postOperasional));

/**
 * @swagger
 *
 * /operasional:
 *  patch:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Operasional ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Operasional"
 *        schema:
 *          $ref: "#/components/schemas/operasionalData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/operasional", callback(patchOperasional));

/**
 * @swagger
 *
 * /operasional:
 *  delete:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Operasional ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/operasional", callback(deleteOperasional));

/**
 * @swagger
 *
 * /operasional/data:
 *  post:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/operasional/data", callback(dataOperasional));

/**
 * @swagger
 *
 * /operasional/data-penumpang-berangkat:
 *  post:
 *    tags:
 *      - "operasional"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post(
  "/operasional/data-penumpang-berangkat",
  callback(dataPenumpangBerangkat)
);

router.post("/operasional/upload/:type", postUploadOperasional);

module.exports = router;
