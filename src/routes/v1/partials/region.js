const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const { postListRegion } = require("../../../app/controllers/region");

/**
 * @swagger
 *
 * /region/list:
 *  post:
 *    tags:
 *      - "region"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "List Region search by village"
 *        schema:
 *          type: "object"
 *          example: {"search": "", "id": false}
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/regionData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/region/list", callback(postListRegion));

module.exports = router;
