const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getEblue,
  dataEblue,
  postEblue,
  patchEblue,
  deleteEblue,
} = require("../../../app/controllers/eblue");

/**
 * @swagger
 *
 * /eblue:
 *  get:
 *    tags:
 *      - "eblue"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Eblue ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/eblueData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/eblue", callback(getEblue));

/**
 * @swagger
 *
 * /eblue:
 *  post:
 *    tags:
 *      - "eblue"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Eblue Baru"
 *        schema:
 *          $ref: "#/components/schemas/eblueData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/eblue", callback(postEblue));

/**
 * @swagger
 *
 * /eblue:
 *  patch:
 *    tags:
 *      - "eblue"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Eblue ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Eblue"
 *        schema:
 *          $ref: "#/components/schemas/eblueData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/eblue", callback(patchEblue));

/**
 * @swagger
 *
 * /eblue:
 *  delete:
 *    tags:
 *      - "eblue"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Eblue ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/eblue", callback(deleteEblue));

/**
 * @swagger
 *
 * /eblue/data:
 *  post:
 *    tags:
 *      - "eblue"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/eblue/data", callback(dataEblue));

module.exports = router;
