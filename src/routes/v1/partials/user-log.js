const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  dataUserLog,
  getUserLog,
} = require("../../../app/controllers/user-log");

/**
 * @swagger
 *
 * /user-log:
 *  get:
 *    tags:
 *      - "user-log"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "User Log ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/userRoleData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user-log", callback(getUserLog));

/**
 * @swagger
 *
 * /user-log/data:
 *  post:
 *    tags:
 *      - "user-log"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, user_id, lat, lng, browser, version, os, platform, type, expired_in</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/user-log/data", callback(dataUserLog));

module.exports = router;
