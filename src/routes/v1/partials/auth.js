const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  postLoginWithPassword,
  postRefreshToken,
  postRegister,
} = require("../../../app/controllers/auth");

/**
 * @swagger
 *
 * /auth/login-with-password:
 *  post:
 *    tags:
 *      - "auth"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Login with Password"
 *        schema:
 *          $ref: "#/components/schemas/loginData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/loginResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/auth/login-with-password", callback(postLoginWithPassword));

/**
 * @swagger
 *
 * /auth/refresh:
 *  post:
 *    tags:
 *      - "auth"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Refresh Token"
 *        schema:
 *          type: "object"
 *          example: {"token": ""}
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/loginResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/auth/refresh", callback(postRefreshToken));

/**
 * @swagger
 *
 * /auth/register:
 *  post:
 *    tags:
 *      - "auth"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Register"
 *        schema:
 *          type: "object"
 *          example: {"name": "", "username": "", "email": "", "password": ""}
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/auth/register", callback(postRegister));

module.exports = router;
