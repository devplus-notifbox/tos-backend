const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getAngkutan,
  dataAngkutan,
  postAngkutan,
  patchAngkutan,
  deleteAngkutan,
} = require("../../../app/controllers/angkutan");

/**
 * @swagger
 *
 * /angkutan:
 *  get:
 *    tags:
 *      - "angkutan"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Angkutan ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/angkutanData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/angkutan", callback(getAngkutan));

/**
 * @swagger
 *
 * /angkutan:
 *  post:
 *    tags:
 *      - "angkutan"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Angkutan Baru"
 *        schema:
 *          $ref: "#/components/schemas/angkutanData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/angkutan", callback(postAngkutan));

/**
 * @swagger
 *
 * /angkutan:
 *  patch:
 *    tags:
 *      - "angkutan"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Angkutan ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Angkutan"
 *        schema:
 *          $ref: "#/components/schemas/angkutanData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/angkutan", callback(patchAngkutan));

/**
 * @swagger
 *
 * /angkutan:
 *  delete:
 *    tags:
 *      - "angkutan"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Angkutan ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/angkutan", callback(deleteAngkutan));

/**
 * @swagger
 *
 * /angkutan/data:
 *  post:
 *    tags:
 *      - "angkutan"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/angkutan/data", callback(dataAngkutan));

module.exports = router;
