const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getUser,
  dataUser,
  postUser,
  patchUser,
  deleteUser,
  getPermission,
  getListRole,
  postSwitchZone,
  postSwitchRole,
  getUserInfo,
  patchProfile,
} = require("../../../app/controllers/user");
const permission = require("../../../app/usecases/user/permission");

/**
 * @swagger
 *
 * /user:
 *  get:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "User ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/userData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user", callback(getUser));

/**
 * @swagger
 *
 * /user/info:
 *  get:
 *    tags:
 *      - "user"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/userData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user/info", callback(getUserInfo));

/**
 * @swagger
 *
 * /user:
 *  post:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah User Baru"
 *        schema:
 *          $ref: "#/components/schemas/userData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/user", callback(postUser));

/**
 * @swagger
 *
 * /user:
 *  patch:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "User ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update User"
 *        schema:
 *          $ref: "#/components/schemas/userData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/user", callback(patchUser));

/**
 * @swagger
 *
 * /user/update-profile:
 *  patch:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update User Profile"
 *        schema:
 *          $ref: "#/components/schemas/userData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/user/update-profile", callback(patchProfile));

/**
 * @swagger
 *
 * /user:
 *  delete:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "User ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/user", callback(deleteUser));

/**
 * @swagger
 *
 * /user/data:
 *  post:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/user/data", callback(dataUser));

/**
 * @swagger
 *
 * /user/permission:
 *  get:
 *    tags:
 *      - "user"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/permissionResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user/permission", callback(getPermission));

/**
 * @swagger
 *
 * /user/list-role:
 *  get:
 *    tags:
 *      - "user"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/listRoleResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user/list-role", callback(getListRole));

/**
 * @swagger
 *
 * /user/switch-zone:
 *  post:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Switch Zone"
 *        schema:
 *          type: "object"
 *          example: {"zone_id": ""}
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: {"to": "/"}
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/user/switch-zone", callback(postSwitchZone));

/**
 * @swagger
 *
 * /user/switch-role:
 *  post:
 *    tags:
 *      - "user"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Switch Zone"
 *        schema:
 *          type: "object"
 *          example: {"role_id": ""}
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: {"to": "/"}
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/user/switch-role", callback(postSwitchRole));

module.exports = router;
