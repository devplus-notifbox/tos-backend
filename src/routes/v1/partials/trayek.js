const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getTrayek,
  dataTrayek,
  postTrayek,
  patchTrayek,
  deleteTrayek,
  postTrayekAngkutan,
} = require("../../../app/controllers/trayek");

/**
 * @swagger
 *
 * /trayek:
 *  get:
 *    tags:
 *      - "trayek"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Trayek ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/trayekData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/trayek", callback(getTrayek));

/**
 * @swagger
 *
 * /trayek:
 *  post:
 *    tags:
 *      - "trayek"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Trayek Baru"
 *        schema:
 *          $ref: "#/components/schemas/trayekData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/trayek", callback(postTrayek));

/**
 * @swagger
 *
 * /trayek:
 *  patch:
 *    tags:
 *      - "trayek"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Trayek ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Trayek"
 *        schema:
 *          $ref: "#/components/schemas/trayekData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/trayek", callback(patchTrayek));

/**
 * @swagger
 *
 * /trayek:
 *  delete:
 *    tags:
 *      - "trayek"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Trayek ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/trayek", callback(deleteTrayek));

/**
 * @swagger
 *
 * /trayek/data:
 *  post:
 *    tags:
 *      - "trayek"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/trayek/data", callback(dataTrayek));

/**
 * @swagger
 *
 * /trayek/add-angkutan:
 *  get:
 *    tags:
 *      - "trayek"
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: {"status": true}
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/trayek/add-angkutan", callback(postTrayekAngkutan));

module.exports = router;
