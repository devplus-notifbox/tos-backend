const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getDipass,
  dataDipass,
  postDipass,
  patchDipass,
  deleteDipass,
} = require("../../../app/controllers/dipass");

/**
 * @swagger
 *
 * /dipass:
 *  get:
 *    tags:
 *      - "dipass"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Dipass ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dipassData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/dipass", callback(getDipass));

/**
 * @swagger
 *
 * /dipass:
 *  post:
 *    tags:
 *      - "dipass"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Dipass Baru"
 *        schema:
 *          $ref: "#/components/schemas/dipassData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/dipass", callback(postDipass));

/**
 * @swagger
 *
 * /dipass:
 *  patch:
 *    tags:
 *      - "dipass"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Dipass ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Dipass"
 *        schema:
 *          $ref: "#/components/schemas/dipassData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/dipass", callback(patchDipass));

/**
 * @swagger
 *
 * /dipass:
 *  delete:
 *    tags:
 *      - "dipass"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Dipass ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/dipass", callback(deleteDipass));

/**
 * @swagger
 *
 * /dipass/data:
 *  post:
 *    tags:
 *      - "dipass"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/dipass/data", callback(dataDipass));

module.exports = router;
