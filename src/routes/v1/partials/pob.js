const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getPob,
  dataPob,
  postPob,
  patchPob,
  deletePob,
  getListPob,
} = require("../../../app/controllers/pob");

/**
 * @swagger
 *
 * /pob:
 *  get:
 *    tags:
 *      - "pob"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Pob ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/pobData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/pob", callback(getPob));

/**
 * @swagger
 *
 * /pob:
 *  post:
 *    tags:
 *      - "pob"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Pob Baru"
 *        schema:
 *          $ref: "#/components/schemas/pobData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/pob", callback(postPob));

/**
 * @swagger
 *
 * /pob:
 *  patch:
 *    tags:
 *      - "pob"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Pob ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Pob"
 *        schema:
 *          $ref: "#/components/schemas/pobData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/pob", callback(patchPob));

/**
 * @swagger
 *
 * /pob:
 *  delete:
 *    tags:
 *      - "pob"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Pob ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/pob", callback(deletePob));

/**
 * @swagger
 *
 * /pob/data:
 *  post:
 *    tags:
 *      - "pob"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/pob/data", callback(dataPob));

module.exports = router;
