const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getTerminal,
  dataTerminal,
  postTerminal,
  patchTerminal,
  deleteTerminal,
} = require("../../../app/controllers/terminal");

/**
 * @swagger
 *
 * /terminal:
 *  get:
 *    tags:
 *      - "terminal"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Terminal ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/terminalData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/terminal", callback(getTerminal));

/**
 * @swagger
 *
 * /terminal:
 *  post:
 *    tags:
 *      - "terminal"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Terminal Baru"
 *        schema:
 *          $ref: "#/components/schemas/terminalData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/terminal", callback(postTerminal));

/**
 * @swagger
 *
 * /terminal:
 *  patch:
 *    tags:
 *      - "terminal"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Terminal ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Terminal"
 *        schema:
 *          $ref: "#/components/schemas/terminalData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/terminal", callback(patchTerminal));

/**
 * @swagger
 *
 * /terminal:
 *  delete:
 *    tags:
 *      - "terminal"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Terminal ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/terminal", callback(deleteTerminal));

/**
 * @swagger
 *
 * /terminal/data:
 *  post:
 *    tags:
 *      - "terminal"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, code, name, phone, address, lat, lng, level, parent, active</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/terminal/data", callback(dataTerminal));

module.exports = router;
