const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  dataDashboardOperasionalKendaraan,
  dataDashboardOperasionalPenumpang,
} = require("../../../app/controllers/dashboard");

/**
 * @swagger
 *
 * /dashboard-operasional-kendaraan:
 *  post:
 *    tags:
 *      - "dashboard"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: ""
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post(
  "/dashboard-operasional-kendaraan",
  callback(dataDashboardOperasionalKendaraan)
);

/**
 * @swagger
 *
 * /dashboard-operasional-penumpang:
 *  post:
 *    tags:
 *      - "dashboard"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: ""
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post(
  "/dashboard-operasional-penumpang",
  callback(dataDashboardOperasionalPenumpang)
);

module.exports = router;
