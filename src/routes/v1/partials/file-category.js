const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getFileCategory,
  dataFileCategory,
  postFileCategory,
  patchFileCategory,
  deleteFileCategory,
  getListFileCategory,
} = require("../../../app/controllers/file-category");

/**
 * @swagger
 *
 * /file-category:
 *  get:
 *    tags:
 *      - "file-category"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "FileCategory ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/fileCategoryData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/file-category", callback(getFileCategory));

/**
 * @swagger
 *
 * /file-category:
 *  post:
 *    tags:
 *      - "file-category"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah FileCategory Baru"
 *        schema:
 *          $ref: "#/components/schemas/fileCategoryData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/file-category", callback(postFileCategory));

/**
 * @swagger
 *
 * /file-category:
 *  patch:
 *    tags:
 *      - "file-category"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "FileCategory ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update FileCategory"
 *        schema:
 *          $ref: "#/components/schemas/fileCategoryData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/file-category", callback(patchFileCategory));

/**
 * @swagger
 *
 * /file-category:
 *  delete:
 *    tags:
 *      - "file-category"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "FileCategory ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/file-category", callback(deleteFileCategory));

/**
 * @swagger
 *
 * /file-category/data:
 *  post:
 *    tags:
 *      - "file-category"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/file-category/data", callback(dataFileCategory));

/**
 * @swagger
 *
 * /file-category/list:
 *  get:
 *    tags:
 *      - "file-category"
 *    responses:
 *      '200':
 *          schema:
 *            type: "object"
 *            example: [{"id":"", "slug":"", "name":""}]
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/file-category/list", callback(getListFileCategory));

module.exports = router;
