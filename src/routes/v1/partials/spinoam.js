const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getSpinoam,
  dataSpinoam,
  postSpinoam,
  patchSpinoam,
  deleteSpinoam,
} = require("../../../app/controllers/spinoam");

/**
 * @swagger
 *
 * /spinoam:
 *  get:
 *    tags:
 *      - "spinoam"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Spinoam ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/spinoamData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/spinoam", callback(getSpinoam));

/**
 * @swagger
 *
 * /spinoam:
 *  post:
 *    tags:
 *      - "spinoam"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Spinoam Baru"
 *        schema:
 *          $ref: "#/components/schemas/spinoamData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/spinoam", callback(postSpinoam));

/**
 * @swagger
 *
 * /spinoam:
 *  patch:
 *    tags:
 *      - "spinoam"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Spinoam ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Spinoam"
 *        schema:
 *          $ref: "#/components/schemas/spinoamData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/spinoam", callback(patchSpinoam));

/**
 * @swagger
 *
 * /spinoam:
 *  delete:
 *    tags:
 *      - "spinoam"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Spinoam ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/spinoam", callback(deleteSpinoam));

/**
 * @swagger
 *
 * /spinoam/data:
 *  post:
 *    tags:
 *      - "spinoam"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/spinoam/data", callback(dataSpinoam));

module.exports = router;
