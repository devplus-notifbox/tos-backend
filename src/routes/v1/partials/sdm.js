const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getSdm,
  dataSdm,
  postSdm,
  patchSdm,
  deleteSdm,
} = require("../../../app/controllers/sdm");

/**
 * @swagger
 *
 * /sdm:
 *  get:
 *    tags:
 *      - "sdm"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Sdm ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/sdmData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/sdm", callback(getSdm));

/**
 * @swagger
 *
 * /sdm:
 *  post:
 *    tags:
 *      - "sdm"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Sdm Baru"
 *        schema:
 *          $ref: "#/components/schemas/sdmData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/sdm", callback(postSdm));

/**
 * @swagger
 *
 * /sdm:
 *  patch:
 *    tags:
 *      - "sdm"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Sdm ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Sdm"
 *        schema:
 *          $ref: "#/components/schemas/sdmData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/sdm", callback(patchSdm));

/**
 * @swagger
 *
 * /sdm:
 *  delete:
 *    tags:
 *      - "sdm"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Sdm ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/sdm", callback(deleteSdm));

/**
 * @swagger
 *
 * /sdm/data:
 *  post:
 *    tags:
 *      - "sdm"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/sdm/data", callback(dataSdm));

module.exports = router;
