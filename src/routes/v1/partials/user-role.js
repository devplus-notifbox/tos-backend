const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getUserRole,
  dataUserRole,
  postUserRole,
  patchUserRole,
  deleteUserRole,
} = require("../../../app/controllers/user-role");

/**
 * @swagger
 *
 * /user-role:
 *  get:
 *    tags:
 *      - "user-role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "User Role ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/userRoleData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/user-role", callback(getUserRole));

/**
 * @swagger
 *
 * /user-role:
 *  post:
 *    tags:
 *      - "user-role"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah UserRole Baru"
 *        schema:
 *          $ref: "#/components/schemas/userRoleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/user-role", callback(postUserRole));

/**
 * @swagger
 *
 * /user-role:
 *  patch:
 *    tags:
 *      - "user-role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "UserRole ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update UserRole"
 *        schema:
 *          $ref: "#/components/schemas/userRoleData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/user-role", callback(patchUserRole));

/**
 * @swagger
 *
 * /user-role:
 *  delete:
 *    tags:
 *      - "user-role"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "UserRole ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/user-role", callback(deleteUserRole));

/**
 * @swagger
 *
 * /user-role/data:
 *  post:
 *    tags:
 *      - "user-role"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/user-role/data", callback(dataUserRole));

module.exports = router;
