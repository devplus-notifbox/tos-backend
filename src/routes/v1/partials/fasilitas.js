const express = require("express");
const callback = require("../../../utils/callback");

const router = express.Router();

const {
  getFasilitas,
  dataFasilitas,
  postFasilitas,
  patchFasilitas,
  deleteFasilitas,
} = require("../../../app/controllers/fasilitas");

/**
 * @swagger
 *
 * /fasilitas:
 *  get:
 *    tags:
 *      - "fasilitas"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Fasilitas ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/fasilitasData"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.get("/fasilitas", callback(getFasilitas));

/**
 * @swagger
 *
 * /fasilitas:
 *  post:
 *    tags:
 *      - "fasilitas"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Tambah Fasilitas Baru"
 *        schema:
 *          $ref: "#/components/schemas/fasilitasData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.post("/fasilitas", callback(postFasilitas));

/**
 * @swagger
 *
 * /fasilitas:
 *  patch:
 *    tags:
 *      - "fasilitas"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Fasilitas ID"
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Update Fasilitas"
 *        schema:
 *          $ref: "#/components/schemas/fasilitasData"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/postResponse"
 *    security:
 *      - authorization:
 *          - "read"
 *          - "write"
 */
router.patch("/fasilitas", callback(patchFasilitas));

/**
 * @swagger
 *
 * /fasilitas:
 *  delete:
 *    tags:
 *      - "fasilitas"
 *    parameters:
 *      - name: "id"
 *        in: "query"
 *        description: "Fasilitas ID"
 *        schema:
 *          type: "string"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/deleteResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.delete("/fasilitas", callback(deleteFasilitas));

/**
 * @swagger
 *
 * /fasilitas/data:
 *  post:
 *    tags:
 *      - "fasilitas"
 *    parameters:
 *      - name: "body"
 *        in: "body"
 *        required: true
 *        description: "Datatable object untuk pagination, search, sorting dan filtering <br/><br/><br/>  <strong>Kolom yg tersedia:</strong> <br/><i>id, slug, name</i>"
 *        schema:
 *          $ref: "#/components/schemas/dataTablePost"
 *    responses:
 *      '200':
 *          schema:
 *              $ref: "#/components/schemas/dataTableResponse"
 *    security:
 *      - authorization:
 *          - "read"
 */
router.post("/fasilitas/data", callback(dataFasilitas));

module.exports = router;
