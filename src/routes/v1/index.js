const express = require("express");
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const callback = require("../../utils/callback");
const authMiddleware = require("../../app/middleware/auth");
const router = express.Router();

const swaggerOptions = require("./swagger-options");
const swaggerDocs = swaggerJsDoc(swaggerOptions);

router.use("/", swaggerUi.serve);
router.get(
  "/",
  swaggerUi.setup(swaggerDocs, {
    customCss: ".topbar{display: none !important;}",
  })
);

router.use(require("./partials/auth"));
router.use(authMiddleware);
router.use(require("./partials/dashboard"));
router.use(require("./partials/module"));
router.use(require("./partials/action"));
router.use(require("./partials/zone"));
router.use(require("./partials/role"));
router.use(require("./partials/user"));
router.use(require("./partials/user-role"));
router.use(require("./partials/user-log"));
router.use(require("./partials/file"));
router.use(require("./partials/file-category"));
router.use(require("./partials/terminal"));
router.use(require("./partials/sdm"));
router.use(require("./partials/fasilitas"));
router.use(require("./partials/pob"));
router.use(require("./partials/trayek"));
router.use(require("./partials/angkutan"));
router.use(require("./partials/region"));
router.use(require("./partials/operasional"));
router.use(require("./partials/dipass"));
router.use(require("./partials/eblue"));
router.use(require("./partials/spinoam"));

module.exports = router;
