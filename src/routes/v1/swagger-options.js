const config = require("../../config/app");
const schemas = {
  permissionResponse: {
    type: "object",
    example: {
      user: ["view", "create", "update", "delete", "change-password"],
      role: ["view", "create", "update", "delete"],
    },
  },
  listRoleResponse: {
    type: "object",
    example: [
      {
        zone_id: "26201546-682c-4bf1-9d0a-8324af031d39",
        zone_name: "Default Zone",
        roles: [
          {
            id: "c2a88b18-4783-4fcc-8192-db32ec9aa297",
            slug: "administrator",
            name: "Administrator",
          },
        ],
      },
    ],
  },
  loginData: {
    type: "object",
    example: {
      email: "demo@demo.com",
      password: "AuxKo5E@",
    },
  },
  loginResponse: {
    type: "object",
    example: {
      token_type: "bearer",
      access_token: "...",
      refresh_token: "...",
      user: {
        id: "",
        email: "",
        name: "",
        username: "",
        activeZone: {
          id: "",
          code: "",
          name: "",
        },
        activeRole: {
          id: "",
          slug: "",
          name: "",
        },
      },
      to: "/",
      expires_in: "0",
    },
  },
  userRoleData: {
    type: "object",
    example: {
      user_id: "98f043e9-b33f-46ce-8bd9-a1ee45af1874",
      zone_id: "26201546-682c-4bf1-9d0a-8324af031d39",
      roles: ["administrator"],
    },
  },
  userData: {
    type: "object",
    example: {
      name: "Administrator 2",
      email: "admin@gmail.com",
      username: "admin",
      phone: "",
      address: "",
      active: true,
    },
  },
  roleData: {
    type: "object",
    example: {
      slug: "admin",
      name: "Administrator",
      default_route: "/",
      permissions: { user: ["create", "update", "delete"] },
    },
  },
  operasionalData: {
    type: "object",
    example: {
      zone_id: "",
      tanggal: "",
      no_kendaraan: "",
      jumlah_penumpang: "",
      tipe: "",
    },
  },
  zoneData: {
    type: "object",
    example: {
      code: "12345",
      name: "Default Zone",
    },
  },
  fasilitasData: {
    type: "object",
    example: {
      zone_id: "",
      nama: "",
      deskripsi: "",
      kategori: "",
      gambar: "",
      aktif: "",
    },
  },
  trayekData: {
    type: "object",
    example: {
      kode: "",
      nama: "",
      deskripsi: "",
      aktif: "",
      terminal: "",
    },
  },
  terminalData: {
    type: "object",
    example: {
      code: "12345",
      name: "Default Zone",
    },
  },
  moduleData: {
    type: "object",
    example: {
      slug: "action",
      name: "Action",
    },
  },
  actionData: {
    type: "object",
    example: {
      slug: "create",
      name: "Create",
    },
  },
  pobData: {
    type: "object",
    example: {
      id: "",
      nama: "",
      no_izin: "",
      telepon: "",
      kode_pos: "",
      village_id: "",
      alamat: "",
      tipe: "",
      aktif: "",
    },
  },
  angkutanData: {
    type: "object",
    example: {
      trayek_id: "",
      pob_id: "",
      no_kendaraan: "",
      no_uji: "",
      tgl_kadaluarsa_uji: "",
      no_kps: "",
      tgl_kadaluarsa_kps: "",
      no_srut: "",
      no_rangka: "",
      no_mesin: "",
      merek: "",
      tahun_buat: "",
      kapasitas: "",
      deskripsi: "",
      aktif: "",
    },
  },
  regionData: {
    type: "object",
    example: [
      {
        id: "31.74.10.1003",
        name: "PETUKANGAN UTARA, PESANGGRAHAN, JAKARTA SELATAN",
        district_name: "PESANGGRAHAN",
        city_name: "JAKARTA SELATAN",
        province_name: "DKI JAKARTA",
        village_name: "PETUKANGAN UTARA",
      },
    ],
  },
  sdmData: {
    type: "object",
    example: {
      zone_id: "",
      nip: "",
      nama: "",
      jabatan: "",
      tipe: "",
      alamat: "",
      kode_pos: "",
      telepon: "",
      village_id: "",
    },
  },
  fileCategoryData: {
    type: "object",
    example: {
      slug: "create",
      name: "Create",
    },
  },
  dipassData: {
    type: "object",
    example: {
      nama: "",
      nama_bus: "",
      no_kursi: "",
      waktu_berangkat: "",
      waktu_tiba: "",
      no_identitas: "",
      rute_asal: "",
      rute_tujuan: "",
    },
  },
  eblueData: {
    type: "object",
    example: {
      nama_pemilik: "",
      alamat_pemilik: "",
      no_registrasi_kendaraan: "",
      no_rangka: "",
      no_mesin: "",
      jenis_kendaraan: "",
      merk: "",
      tipe: "",
      keterangan_hasil_uji: "",
      masa_berlaku: "",
      petugas_penguji: "",
      nrp_petugas_penguji: "",
      unit_pelaksana_teknis: "",
    },
  },
  spinoamData: {
    type: "object",
    example: {
      perusahaan_id: "",
      jenis_pelayanan: "",
      nama_perusahaan: "",
      alamat: "",
      no_sk: "",
      tgl_exp_sk: "",
      no_uji: "",
      tgl_exp_uji: "",
      no_kps: "",
      tgl_exp_kps: "",
      no_rangka: "",
      no_mesin: "",
      merek: "",
      tahun: "",
      no_kendaraan: "",
    },
  },
  postResponse: {
    type: "object",
    example: {
      id: "54de80f4-fba4-4fcc-874f-2672a03f468e",
    },
  },
  deleteResponse: {
    type: "object",
    example: {
      deleted: 1,
    },
  },
  dataTablePost: {
    type: "object",
    example: {
      columns: [
        {
          data: "column_name",
        },
      ],
      order: {
        column: "column_name",
        dir: "asc",
      },
      page: 1,
      perPage: 10,
    },
  },
  dataTableResponse: {
    type: "object",
    example: {
      recordsTotal: 100,
      data: [],
    },
  },
};

module.exports = {
  swaggerDefinition: {
    info: {
      version: "version 1.0.0",
      title: "Framework V3",
      description: "Framework V3",
      contact: {
        name: "Barkah Hadi",
      },
      servers: [config.url],
    },
    basePath: "/api/v1",
    schemes: ["http", "https"],
    produces: ["application/json"],
    components: {
      schemas: schemas,
    },
    securityDefinitions: {
      authorization: {
        type: "apiKey",
        name: "Authorization",
        in: "header",
      },
    },
  },
  // ['.routes/*.js']
  apis: [
    "./src/routes/v1/index.js",
    "./src/routes/v1/partials/auth.js",
    "./src/routes/v1/partials/module.js",
    "./src/routes/v1/partials/action.js",
    "./src/routes/v1/partials/zone.js",
    "./src/routes/v1/partials/role.js",
    "./src/routes/v1/partials/user.js",
    "./src/routes/v1/partials/user-role.js",
    "./src/routes/v1/partials/user-log.js",
    "./src/routes/v1/partials/file.js",
    "./src/routes/v1/partials/file-category.js",
    "./src/routes/v1/partials/dashboard.js",
    "./src/routes/v1/partials/terminal.js",
    "./src/routes/v1/partials/sdm.js",
    "./src/routes/v1/partials/fasilitas.js",
    "./src/routes/v1/partials/pob.js",
    "./src/routes/v1/partials/trayek.js",
    "./src/routes/v1/partials/angkutan.js",
    "./src/routes/v1/partials/region.js",
    "./src/routes/v1/partials/operasional.js",
    "./src/routes/v1/partials/dipass.js",
    "./src/routes/v1/partials/eblue.js",
    "./src/routes/v1/partials/spinoam.js",
  ],
};
