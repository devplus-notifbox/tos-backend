require("dotenv").config();
var readline = require("readline");
const {
  mProvince,
  mCity,
  mDistrict,
  mVillage,
  mZone,
  mTerminal,
  mPob,
  mAngkutan,
} = require("../app/models");
const db = require("../utils/database");
// const dataProvince = require("./provinces_202108141735.json");
// const dataCity = require("./cities_202108141736.json");
// const dataDistrict = require("./districts_202108141737.json");
// const dataVillage = require("./villages_202108141737.json");
// const dataTerminal = require("./terminal.json");
// const dataPerusahaan = require("./perusahaan.json");
const dataKendaraan = require("./kendaraan.json");
const moment = require("moment");

const { forEach } = require("lodash");
setTimeout(() => {
  //   console.log(dataProvince);
  // Insert Province
  //   var i = 0;
  //   forEach(dataProvince.provinces, async (val) => {
  //     await new mProvince({
  //       id: val.id,
  //       name: val.name,
  //     }).save();
  //     console.log(++i);
  //   });
  //   console.log("Done");
  // Insert City
  //   var i = 0;
  //   forEach(dataCity.cities, async (val) => {
  //     await new mCity({
  //       id: val.id,
  //       province_id: val.provinceId,
  //       name: val.name,
  //     }).save();
  //     console.log(++i);
  //   });
  //   console.log("Done");
  // Insert District
  //   var i = 0;
  //   forEach(dataDistrict.districts, async (val) => {
  //     await new mDistrict({
  //       id: val.id,
  //       city_id: val.cityId,
  //       name: val.name,
  //     }).save();
  //     console.log(++i);
  //   });
  //   console.log("Done");
  // Insert Village
  // var i = 0;
  // forEach(dataVillage.villages, async (val) => {
  //   if (i > 65000) {
  //     const exists = await mVillage.findOne({ where: { id: val.id } });
  //     if (exists == null) {
  //       await new mVillage({
  //         id: val.id,
  //         district_id: val.districtId,
  //         name: val.name,
  //         postal_code: val.postalCode,
  //       }).save();
  //     }
  //   }
  //   console.log(++i);
  // });
  // console.log("Done");
  // Insert Terminal
  // var i = 0;
  // forEach(dataTerminal, async (val) => {
  //   const zone = await new mZone({
  //     code: ++i,
  //     name: val.nama,
  //     level: "terminal",
  //   }).save();
  //   const terminal = await new mTerminal({
  //     zone_id: zone.id,
  //     tipe: val.tipe,
  //   }).save();
  // });
  // forEach(dataPerusahaan, async (val) => {
  //   // console.log({
  //   //   kode: val.perusahaan_id,
  //   //   nama: val.nama_perusahaan,
  //   //   nama_direktur: val.nama_direktur,
  //   //   no_izin: val.no_sk,
  //   //   tgl_kadaluarsa_izin: moment(val.tgl_exp_sk).format("YYYY-MM-DD"),
  //   //   jumlah_kendaraan: val.jml_kendaraan,
  //   //   alamat: val.alamat,
  //   //   tipe: val.jenis_pelayanan.toLowerCase(),
  //   //   aktif: true,
  //   // });
  //   await new mPob({
  //     kode: val.perusahaan_id,
  //     nama: val.nama_perusahaan,
  //     nama_direktur: val.nama_direktur,
  //     no_izin: val.no_sk,
  //     tgl_kadaluarsa_izin: moment(val.tgl_exp_sk).format("YYYY-MM-DD"),
  //     jumlah_kendaraan: val.jml_kendaraan,
  //     alamat: val.alamat,
  //     tipe: val.jenis_pelayanan.toLowerCase(),
  //     aktif: true,
  //   }).save();
  // });

  forEach(dataKendaraan, async (val) => {
    const pob = await mPob.findOne({ where: { kode: val.perusahaan_id } });
    // console.log({
    //   pob_id: pob ? pob.id : null,
    //   no_kendaraan: val.noken,
    //   no_uji: val.no_uji,
    //   tgl_kadaluarsa_uji: moment(val.tgl_exp_uji).format("YYYY-MM-DD"),
    //   no_kps: val.no_kps,
    //   tgl_kadaluarsa_kps: moment(val.tgl_exp_kps).format("YYYY-MM-DD"),
    //   no_srut: val.no_srut,
    //   no_rangka: val.no_rangka,
    //   no_mesin: val.no_mesin,
    //   merek: val.merek,
    //   tahun_buat: val.tahun,
    //   kapasitas: val.seat,
    //   deskripsi: "",
    //   aktif: true,
    // });
    const dateExpUji = moment(val.tgl_exp_uji);
    const dateExpKps = moment(val.tgl_exp_kps);
    await new mAngkutan({
      pob_id: pob ? pob.id : null,
      no_kendaraan: val.noken,
      no_uji: val.no_uji,
      tgl_kadaluarsa_uji: dateExpUji.isValid()
        ? moment(val.tgl_exp_uji).format("YYYY-MM-DD")
        : null,
      no_kps: val.no_kps,
      tgl_kadaluarsa_kps: dateExpKps.isValid()
        ? moment(val.tgl_exp_kps).format("YYYY-MM-DD")
        : null,
      no_srut: val.no_srut,
      no_rangka: val.no_rangka,
      no_mesin: val.no_mesin,
      merek: val.merek,
      tahun_buat: val.tahun,
      kapasitas: val.seat,
      deskripsi: "",
      aktif: true,
    }).save();
  });
}, 500);
