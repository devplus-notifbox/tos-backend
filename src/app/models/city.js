"use strict";

module.exports = (sequelize, DataTypes) => {
  const city = sequelize.define(
    "city",
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      province_id: DataTypes.STRING,
      name: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return city;
};
