"use strict";

module.exports = (sequelize, DataTypes) => {
  const fileCategory = sequelize.define(
    "file_category",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      slug: DataTypes.STRING,
      name: DataTypes.STRING,
      allowed_extension: DataTypes.JSON,
      max_file_size: DataTypes.INTEGER,
      active: DataTypes.BOOLEAN,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return fileCategory;
};
