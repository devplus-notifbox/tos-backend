"use strict";

module.exports = (sequelize, DataTypes) => {
  const province = sequelize.define(
    "province",
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      name: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return province;
};
