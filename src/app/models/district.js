"use strict";

module.exports = (sequelize, DataTypes) => {
  const district = sequelize.define(
    "district",
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      city_id: DataTypes.STRING,
      name: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return district;
};
