"use strict";

module.exports = (sequelize, DataTypes) => {
  const village = sequelize.define(
    "village",
    {
      id: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      district_id: DataTypes.STRING,
      name: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return village;
};
