"use strict";

module.exports = (sequelize, DataTypes) => {
  const operasional = sequelize.define(
    "operasional",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      zone_id: DataTypes.UUID,
      zone_id_tujuan: DataTypes.UUID,
      user_id: DataTypes.UUID,
      tanggal: DataTypes.DATE,
      angkutan_id: DataTypes.UUID,
      jumlah_penumpang: DataTypes.INTEGER,
      jumlah_penumpang_2: DataTypes.INTEGER,
      tipe: DataTypes.STRING,
      status_spinoam: DataTypes.BOOLEAN,
      status_eblue: DataTypes.BOOLEAN,
      note: DataTypes.TEXT,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return operasional;
};
