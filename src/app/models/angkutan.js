"use strict";

module.exports = (sequelize, DataTypes) => {
  const angkutan = sequelize.define(
    "angkutan",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      trayek_id: DataTypes.UUID,
      pob_id: DataTypes.UUID,
      tipe: DataTypes.STRING,
      masa_berlaku: DataTypes.DATEONLY,
      no_kendaraan: DataTypes.STRING,
      no_uji: DataTypes.STRING,
      tgl_kadaluarsa_uji: DataTypes.DATEONLY,
      no_kps: DataTypes.STRING,
      tgl_kadaluarsa_kps: DataTypes.DATEONLY,
      no_srut: DataTypes.STRING,
      no_rangka: DataTypes.STRING,
      no_mesin: DataTypes.STRING,
      merek: DataTypes.STRING,
      tahun_buat: DataTypes.INTEGER,
      jenis_kendaraan: DataTypes.STRING,
      kapasitas: DataTypes.INTEGER,
      deskripsi: DataTypes.TEXT,
      aktif: DataTypes.BOOLEAN,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return angkutan;
};
