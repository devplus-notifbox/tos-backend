"use strict";

module.exports = (sequelize, DataTypes) => {
  const eblue = sequelize.define(
    "eblue",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      nama_pemilik: DataTypes.STRING,
      alamat_pemilik: DataTypes.TEXT,
      no_registrasi_kendaraan: DataTypes.STRING,
      no_rangka: DataTypes.STRING,
      no_mesin: DataTypes.STRING,
      jenis_kendaraan: DataTypes.STRING,
      merk: DataTypes.STRING,
      tipe: DataTypes.STRING,
      keterangan_hasil_uji: DataTypes.TEXT,
      masa_berlaku: DataTypes.DATE,
      petugas_penguji: DataTypes.STRING,
      nrp_petugas_penguji: DataTypes.STRING,
      unit_pelaksana_teknis: DataTypes.STRING,
      created_by: DataTypes.STRING,
      updated_by: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return eblue;
};
