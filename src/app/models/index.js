const Sequelize = require("sequelize");
const db = require("../../utils/database");
const sequelize = require("sequelize");

const User = require("./user");
const Role = require("./role");
const Zone = require("./zone");
const UserRole = require("./user-role");
const UserLog = require("./user-log");
const Province = require("./province");
const City = require("./city");
const District = require("./district");
const Village = require("./village");
const Module = require("./module");
const Action = require("./action");
const FileCategory = require("./file-category");
const File = require("./file");
const Terminal = require("./terminal");
const Sdm = require("./sdm");
const Fasilitas = require("./fasilitas");
const Pob = require("./pob");
const Trayek = require("./trayek");
const Angkutan = require("./angkutan");
const Operasional = require("./operasional");
const Dipass = require("./dipass");
const Eblue = require("./eblue");
const Spinoam = require("./eblue");
const DashboardOperasionalKendaraan = require("./dashboard-operasional-kendaraan");
const DashboardOperasionalPenumpang = require("./dashboard-operasional-penumpang");
const PenumpangBerangkat = require("./penumpang-berangkat");

const mUser = User(db, Sequelize);
const mRole = Role(db, Sequelize);
const mZone = Zone(db, Sequelize);
const mUserRole = UserRole(db, Sequelize);
const mUserLog = UserLog(db, Sequelize);
const mProvince = Province(db, Sequelize);
const mCity = City(db, Sequelize);
const mDistrict = District(db, Sequelize);
const mVillage = Village(db, Sequelize);
const mModule = Module(db, Sequelize);
const mAction = Action(db, Sequelize);
const mFileCategory = FileCategory(db, Sequelize);
const mFile = File(db, Sequelize);
const mTerminal = Terminal(db, Sequelize);
const mSdm = Sdm(db, Sequelize);
const mFasilitas = Fasilitas(db, Sequelize);
const mPob = Pob(db, Sequelize);
const mTrayek = Trayek(db, Sequelize);
const mAngkutan = Angkutan(db, Sequelize);
const mOperasional = Operasional(db, Sequelize);
const mDipass = Dipass(db, Sequelize);
const mEblue = Eblue(db, Sequelize);
const mSpinoam = Spinoam(db, Sequelize);
const mDashboardOperasionalKendaraan = DashboardOperasionalKendaraan(
  db,
  sequelize
);
const mDashboardOperasionalPenumpang = DashboardOperasionalPenumpang(
  db,
  sequelize
);
const mPenumpangBerangkat = PenumpangBerangkat(db, sequelize);

// Define Relations
mUser.hasMany(mUserRole, { foreignKey: "user_id" });
mZone.hasMany(mUserRole, { foreignKey: "zone_id" });

mUser.belongsTo(mRole, { foreignKey: "active_role" });
mUser.belongsTo(mZone, { foreignKey: "active_zone" });

mUserRole.belongsTo(mUser, {
  foreignKey: "user_id",
});
mUserRole.belongsTo(mZone, {
  foreignKey: "zone_id",
});

mUserLog.belongsTo(mUser, {
  foreignKey: "user_id",
});

mCity.belongsTo(mProvince, {
  foreignKey: "province_id",
});

mDistrict.belongsTo(mCity, {
  foreignKey: "city_id",
});

mVillage.belongsTo(mDistrict, {
  foreignKey: "district_id",
});

mFileCategory.hasMany(mFile, { foreignKey: "file_category_id" });
mFile.belongsTo(mFileCategory, { foreignKey: "file_category_id" });

mZone.hasOne(mTerminal, { foreignKey: "zone_id" });
mTerminal.belongsTo(mZone, { foreignKey: "zone_id" });

mAngkutan.belongsTo(mTrayek, { foreignKey: "trayek_id" });
mAngkutan.belongsTo(mPob, { foreignKey: "pob_id" });

mOperasional.belongsTo(mZone, { foreignKey: "zone_id" });
mOperasional.belongsTo(mAngkutan, { foreignKey: "angkutan_id" });
mOperasional.belongsTo(mUser, { foreignKey: "user_id" });
mOperasional.belongsTo(mZone, {
  foreignKey: "zone_id_tujuan",
  as: "zone_tujuan",
});

async function authenticate() {
  try {
    await db.authenticate();
    console.log("Connection has been established successfully.");
  } catch (error) {
    console.error("Unable to connect to the database:", error);
  }
}
authenticate();

module.exports = {
  mUser,
  mRole,
  mZone,
  mUserRole,
  mUserLog,
  mModule,
  mAction,
  mFileCategory,
  mFile,
  mTerminal,
  mSdm,
  mFasilitas,
  mPob,
  mTrayek,
  mAngkutan,
  mProvince,
  mCity,
  mDistrict,
  mVillage,
  mOperasional,
  mDipass,
  mEblue,
  mSpinoam,
  mDashboardOperasionalKendaraan,
  mDashboardOperasionalPenumpang,
  mPenumpangBerangkat,
};
