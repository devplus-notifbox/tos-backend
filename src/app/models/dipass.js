"use strict";

module.exports = (sequelize, DataTypes) => {
  const dipass = sequelize.define(
    "dipass",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      nama: DataTypes.STRING,
      nama_bus: DataTypes.STRING,
      no_kursi: DataTypes.STRING,
      waktu_berangkat: DataTypes.DATE,
      waktu_tiba: DataTypes.DATE,
      no_identitas: DataTypes.STRING,
      rute_asal: DataTypes.STRING,
      rute_tujuan: DataTypes.STRING,
      created_by: DataTypes.STRING,
      updated_by: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return dipass;
};
