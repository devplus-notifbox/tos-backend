"use strict";

module.exports = (sequelize, DataTypes) => {
  const terminal = sequelize.define(
    "terminal",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      zone_id: DataTypes.UUID,
      deskripsi: DataTypes.TEXT,
      gambar: DataTypes.JSON,
      luas_lahan: DataTypes.DOUBLE,
      luas_bangunan: DataTypes.DOUBLE,
      luas_area_pengembangan: DataTypes.STRING,
      tipe: DataTypes.STRING,
      pob: DataTypes.JSON,
      status_p3d: DataTypes.STRING,
      sk_penetapan_lokasi: DataTypes.JSON,
      bast: DataTypes.JSON,
      sertifikat_tanah: DataTypes.JSON,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return terminal;
};
