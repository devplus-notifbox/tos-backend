"use strict";

module.exports = (sequelize, DataTypes) => {
  const file = sequelize.define(
    "file",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: DataTypes.STRING,
      file_category_id: DataTypes.UUID,
      type: DataTypes.STRING,
      size: DataTypes.INTEGER,
      extension: DataTypes.STRING,
      path: DataTypes.STRING,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return file;
};
