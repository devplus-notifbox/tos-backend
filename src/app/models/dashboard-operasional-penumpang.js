"use strict";

module.exports = (sequelize, DataTypes) => {
  const dashboardOperasionalKendaraan = sequelize.define(
    "dashboard_operasional_penumpang",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      tanggal: DataTypes.DATEONLY,
      akap_naik: DataTypes.INTEGER,
      akap_turun: DataTypes.INTEGER,
      akap_tiba: DataTypes.INTEGER,
      akap_berangkat: DataTypes.INTEGER,
      akdp_naik: DataTypes.INTEGER,
      akdp_turun: DataTypes.INTEGER,
      akdp_tiba: DataTypes.INTEGER,
      akdp_berangkat: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  return dashboardOperasionalKendaraan;
};
