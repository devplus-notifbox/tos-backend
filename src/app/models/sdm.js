"use strict";

module.exports = (sequelize, DataTypes) => {
  const sdm = sequelize.define(
    "sdm",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      zone_id: DataTypes.UUID,
      nip: DataTypes.STRING,
      nama: DataTypes.STRING,
      jabatan: DataTypes.STRING,
      tipe: DataTypes.STRING,
      alamat: DataTypes.STRING,
      kode_pos: DataTypes.STRING,
      telepon: DataTypes.STRING,
      village_id: DataTypes.STRING,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return sdm;
};
