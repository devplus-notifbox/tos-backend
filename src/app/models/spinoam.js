"use strict";

module.exports = (sequelize, DataTypes) => {
  const spinoam = sequelize.define(
    "spinoam",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      perusahaan_id: DataTypes.STRING,
      jenis_pelayanan: DataTypes.STRING,
      nama_perusahaan: DataTypes.STRING,
      alamat: DataTypes.DATE,
      no_sk: DataTypes.DATE,
      tgl_exp_sk: DataTypes.DATE,
      no_uji: DataTypes.STRING,
      tgl_exp_uji: DataTypes.DATE,
      no_kps: DataTypes.STRING,
      tgl_exp_kps: DataTypes.DATE,
      no_rangka: DataTypes.STRING,
      no_mesin: DataTypes.STRING,
      merek: DataTypes.STRING,
      tahun: DataTypes.INTEGER,
      no_kendaraan: DataTypes.STRING,
      created_by: DataTypes.STRING,
      updated_by: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return spinoam;
};
