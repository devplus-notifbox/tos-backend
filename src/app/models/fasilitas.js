"use strict";

module.exports = (sequelize, DataTypes) => {
  const fasilitas = sequelize.define(
    "fasilitas",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      zone_id: DataTypes.UUID,
      nama: DataTypes.STRING,
      deskripsi: DataTypes.TEXT,
      kategori: DataTypes.STRING,
      gambar: DataTypes.JSON,
      aktif: DataTypes.BOOLEAN,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return fasilitas;
};
