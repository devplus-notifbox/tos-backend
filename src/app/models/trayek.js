"use strict";

module.exports = (sequelize, DataTypes) => {
  const trayek = sequelize.define(
    "trayek",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      kode: DataTypes.STRING,
      nama: DataTypes.STRING,
      deskripsi: DataTypes.TEXT,
      aktif: DataTypes.BOOLEAN,
      terminal: DataTypes.JSON,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return trayek;
};
