"use strict";

module.exports = (sequelize, DataTypes) => {
  const dashboardOperasionalKendaraan = sequelize.define(
    "dashboard_operasional_kendaraan",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      tanggal: DataTypes.DATEONLY,
      akap_in: DataTypes.INTEGER,
      akap_out: DataTypes.INTEGER,
      akdp_in: DataTypes.INTEGER,
      akdp_out: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  return dashboardOperasionalKendaraan;
};
