"use strict";

module.exports = (sequelize, DataTypes) => {
  const penumpangBerangkat = sequelize.define(
    "penumpang_berangkat",
    {
      terminal_id: DataTypes.UUID,
      terminal: DataTypes.STRING,
      tanggal: DataTypes.DATE,
      no_kendaraan: DataTypes.STRING,
      tiba: DataTypes.STRING,
      turun: DataTypes.STRING,
      naik: DataTypes.STRING,
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  );

  return penumpangBerangkat;
};
