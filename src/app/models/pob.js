"use strict";

module.exports = (sequelize, DataTypes) => {
  const pob = sequelize.define(
    "pob",
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      kode: DataTypes.STRING,
      nama: DataTypes.STRING,
      nama_direktur: DataTypes.STRING,
      no_izin: DataTypes.STRING,
      tgl_kadaluarsa_izin: DataTypes.DATEONLY,
      jumlah_kendaraan: DataTypes.INTEGER,
      telepon: DataTypes.STRING,
      kode_pos: DataTypes.STRING,
      village_id: DataTypes.STRING,
      alamat: DataTypes.TEXT,
      tipe: DataTypes.STRING,
      aktif: DataTypes.BOOLEAN,
      created_by: DataTypes.UUID,
      updated_by: DataTypes.UUID,
    },
    {
      freezeTableName: true,
      timestamps: true,
      createdAt: "created_at",
      updatedAt: "updated_at",
    }
  );

  return pob;
};
