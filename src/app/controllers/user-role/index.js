const {
  singleUserRole,
  dataTableUserRole,
  addUserRole,
  editUserRole,
  removeUserRole,
} = require("../../usecases/user-role");

const makeGetUserRole = require("./get-user-role");
const makePostUserRole = require("./post-user-role");
const makePatchUserRole = require("./patch-user-role");
const makeDeleteUserRole = require("./delete-user-role");
const makeDataUserRole = require("./data-user-role");

const getUserRole = makeGetUserRole({ singleUserRole });
const postUserRole = makePostUserRole({ addUserRole });
const patchUserRole = makePatchUserRole({ editUserRole });
const deleteUserRole = makeDeleteUserRole({ removeUserRole });
const dataUserRole = makeDataUserRole({ dataTableUserRole });

module.exports = Object.freeze({
  getUserRole,
  dataUserRole,
  postUserRole,
  patchUserRole,
  deleteUserRole,
});
