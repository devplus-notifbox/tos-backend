module.exports = function makeDataUserRole({ dataTableUserRole }) {
  return async function dataUserRole(request) {
    try {
      if (!(await request.user.hasAccess("user-role", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const userId = request.query.id || null;
      const data = await dataTableUserRole({
        params: request.body,
        userId,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
