module.exports = function makeDataDashboardOperasionalPenumpang({
  dashboardOperasionalPenumpang,
}) {
  return async function dataDashboardOperasionalPenumpang(request) {
    try {
      if (!(await request.user.hasAccess("dashboard", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dashboardOperasionalPenumpang({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
