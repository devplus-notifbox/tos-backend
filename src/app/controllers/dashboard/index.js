const {
  dashboardOperasionalKendaraan,
  dashboardOperasionalPenumpang,
} = require("../../usecases/dashboard");

const makeDataDashboardOperasionalKendaraan = require("./data-dashboard-operasional-kendaraan");
const makeDataDashboardOperasionalPenumpang = require("./data-dashboard-operasional-penumpang");

const dataDashboardOperasionalKendaraan = makeDataDashboardOperasionalKendaraan(
  {
    dashboardOperasionalKendaraan,
  }
);

const dataDashboardOperasionalPenumpang = makeDataDashboardOperasionalPenumpang(
  { dashboardOperasionalPenumpang }
);

module.exports = Object.freeze({
  dataDashboardOperasionalKendaraan,
  dataDashboardOperasionalPenumpang,
});
