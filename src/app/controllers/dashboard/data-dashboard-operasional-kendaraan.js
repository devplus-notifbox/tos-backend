module.exports = function makeDataDashboardOperasionalKendaraan({
  dashboardOperasionalKendaraan,
}) {
  return async function dataDashboardOperasionalKendaraan(request) {
    try {
      if (!(await request.user.hasAccess("dashboard", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dashboardOperasionalKendaraan({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
