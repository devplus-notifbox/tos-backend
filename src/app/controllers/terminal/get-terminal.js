module.exports = function makeGetTerminal({ singleZone, singleTerminal }) {
  return async function getTerminal(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      var data = await singleZone({
        id,
      });

      var dataTerminal = await singleTerminal({
        zone_id: id,
      });
      delete dataTerminal.id;
      data = {
        ...data,
        ...dataTerminal,
      };

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
