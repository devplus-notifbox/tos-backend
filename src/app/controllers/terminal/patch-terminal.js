module.exports = function makePatchTerminal({ editZone, editTerminal }) {
  return async function patchTerminal(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;
      changes.level = "terminal";

      const patched = await editZone({
        id: request.query.id,
        ...changes,
      });

      const patchedTerminal = await editTerminal({
        zone_id: patched.id,
        ...changes,
      });

      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
