module.exports = function makePostTerminal({ addZone, addTerminal }) {
  return async function postTerminal(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = request.body;
      // console.log(data);
      const postedZone = await addZone({
        data: { ...data, level: "terminal" },
      });

      const postedTerminal = await addTerminal({
        data: { ...data, zone_id: postedZone.id },
      });

      return {
        statusCode: 201,
        body: postedZone,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
