module.exports = function makeDataTerminal({ dataTableTerminal }) {
  return async function dataTerminal(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableTerminal({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
