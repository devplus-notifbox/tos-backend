const {
  dataTableTerminal,
  addTerminal,
  editTerminal,
  singleTerminal,
} = require("../../usecases/terminal");
const {
  addZone,
  editZone,
  removeZone,
  singleZone,
} = require("../../usecases/zone");

const makeDataTerminal = require("./data-terminal");
const makeGetTerminal = require("./get-terminal");
const makePostTerminal = require("./post-terminal");
const makePatchTerminal = require("./patch-terminal");
const makeDeleteTerminal = require("./delete-terminal");

const dataTerminal = makeDataTerminal({ dataTableTerminal });
const getTerminal = makeGetTerminal({ singleZone, singleTerminal });
const postTerminal = makePostTerminal({ addZone, addTerminal });
const patchTerminal = makePatchTerminal({ editZone, editTerminal });
const deleteTerminal = makeDeleteTerminal({ removeZone });

module.exports = Object.freeze({
  dataTerminal,
  getTerminal,
  postTerminal,
  patchTerminal,
  deleteTerminal,
});
