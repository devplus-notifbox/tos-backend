const {
  singleDipass,
  dataTableDipass,
  addDipass,
  editDipass,
  removeDipass,
} = require("../../usecases/dipass");

const makeGetDipass = require("./get-dipass");
const makePostDipass = require("./post-dipass");
const makePatchDipass = require("./patch-dipass");
const makeDeleteDipass = require("./delete-dipass");
const makeDataDipass = require("./data-dipass");

const getDipass = makeGetDipass({ singleDipass });
const postDipass = makePostDipass({ addDipass });
const patchDipass = makePatchDipass({ editDipass });
const deleteDipass = makeDeleteDipass({ removeDipass });
const dataDipass = makeDataDipass({ dataTableDipass });

module.exports = Object.freeze({
  getDipass,
  dataDipass,
  postDipass,
  patchDipass,
  deleteDipass,
});
