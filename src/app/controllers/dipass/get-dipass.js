module.exports = function makeGetDipass({ singleDipass }) {
  return async function getDipass(request) {
    try {
      if (!(await request.user.hasAccess("dipass", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const data = await singleDipass({
        id,
      });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
