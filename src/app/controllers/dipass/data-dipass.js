module.exports = function makeDataDipass({ dataTableDipass }) {
  return async function dataDipass(request) {
    try {
      if (!(await request.user.hasAccess("dipass", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableDipass({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
