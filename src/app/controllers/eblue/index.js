const {
  singleEblue,
  dataTableEblue,
  addEblue,
  editEblue,
  removeEblue,
} = require("../../usecases/eblue");

const makeGetEblue = require("./get-eblue");
const makePostEblue = require("./post-eblue");
const makePatchEblue = require("./patch-eblue");
const makeDeleteEblue = require("./delete-eblue");
const makeDataEblue = require("./data-eblue");

const getEblue = makeGetEblue({ singleEblue });
const postEblue = makePostEblue({ addEblue });
const patchEblue = makePatchEblue({ editEblue });
const deleteEblue = makeDeleteEblue({ removeEblue });
const dataEblue = makeDataEblue({ dataTableEblue });

module.exports = Object.freeze({
  getEblue,
  dataEblue,
  postEblue,
  patchEblue,
  deleteEblue,
});
