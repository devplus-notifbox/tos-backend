module.exports = function makePatchEblue({ editEblue }) {
  return async function patchEblue(request) {
    try {
      if (!(await request.user.hasAccess("eblue", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;

      const patched = await editEblue({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
