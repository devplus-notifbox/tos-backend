module.exports = function makeDeleteEblue({ removeEblue }) {
  return async function deleteEblue(request) {
    try {
      if (!(await request.user.hasAccess("eblue", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeEblue({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
