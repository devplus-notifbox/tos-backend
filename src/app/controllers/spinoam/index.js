const {
  singleSpinoam,
  dataTableSpinoam,
  addSpinoam,
  editSpinoam,
  removeSpinoam,
} = require("../../usecases/spinoam");

const makeGetSpinoam = require("./get-spinoam");
const makePostSpinoam = require("./post-spinoam");
const makePatchSpinoam = require("./patch-spinoam");
const makeDeleteSpinoam = require("./delete-spinoam");
const makeDataSpinoam = require("./data-spinoam");

const getSpinoam = makeGetSpinoam({ singleSpinoam });
const postSpinoam = makePostSpinoam({ addSpinoam });
const patchSpinoam = makePatchSpinoam({ editSpinoam });
const deleteSpinoam = makeDeleteSpinoam({ removeSpinoam });
const dataSpinoam = makeDataSpinoam({ dataTableSpinoam });

module.exports = Object.freeze({
  getSpinoam,
  dataSpinoam,
  postSpinoam,
  patchSpinoam,
  deleteSpinoam,
});
