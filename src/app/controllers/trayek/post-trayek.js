module.exports = function makePostTrayek({ addTrayek }) {
  return async function postTrayek(request) {
    try {
      if (!(await request.user.hasAccess("trayek", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = request.body;
      // console.log(data);
      const posted = await addTrayek({
        data,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
