module.exports = function makeGetTrayek({ singleTrayek }) {
  return async function getTrayek(request) {
    try {
      if (!(await request.user.hasAccess("trayek", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const data = await singleTrayek({
        id,
      });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
