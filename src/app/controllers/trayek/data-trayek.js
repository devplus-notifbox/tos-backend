module.exports = function makeDataTrayek({ dataTableTrayek }) {
  return async function dataTrayek(request) {
    try {
      // if (!(await request.user.hasAccess("trayek", "view"))) {
      //   throw new Error("You don't have permission to access this module!");
      // }

      const data = await dataTableTrayek({
        params: request.body,
        terminal: request.query.terminal || null,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
