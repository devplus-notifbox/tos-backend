const {
  singleTrayek,
  dataTableTrayek,
  addTrayek,
  editTrayek,
  removeTrayek,
  addTrayekAngkutan,
} = require("../../usecases/trayek");

const makeGetTrayek = require("./get-trayek");
const makePostTrayek = require("./post-trayek");
const makePatchTrayek = require("./patch-trayek");
const makeDeleteTrayek = require("./delete-trayek");
const makeDataTrayek = require("./data-trayek");
const makePostTrayekAngkutan = require("./post-trayek-angkutan");

const getTrayek = makeGetTrayek({ singleTrayek });
const postTrayek = makePostTrayek({ addTrayek });
const patchTrayek = makePatchTrayek({ editTrayek });
const deleteTrayek = makeDeleteTrayek({ removeTrayek });
const dataTrayek = makeDataTrayek({ dataTableTrayek });
const postTrayekAngkutan = makePostTrayekAngkutan({ addTrayekAngkutan });

module.exports = Object.freeze({
  getTrayek,
  dataTrayek,
  postTrayek,
  patchTrayek,
  deleteTrayek,
  postTrayekAngkutan,
});
