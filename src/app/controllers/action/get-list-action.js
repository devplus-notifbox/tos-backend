module.exports = function makeGetListAction({ listAction }) {
  return async function getListAction(request) {
    try {
      const data = await listAction();
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
