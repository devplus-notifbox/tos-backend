module.exports = function makePostAction({ addAction }) {
  return async function postAction(request) {
    try {
      if (!(await request.user.hasAccess("action", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = request.body;
      // console.log(data);
      const posted = await addAction({
        data,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
