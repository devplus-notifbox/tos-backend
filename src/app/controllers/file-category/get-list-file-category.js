module.exports = function makeGetListFileCategory({ listFileCategory }) {
  return async function getListFileCategory(request) {
    try {
      const data = await listFileCategory();
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
