const {
  singleFileCategory,
  dataTableFileCategory,
  addFileCategory,
  editFileCategory,
  removeFileCategory,
  listFileCategory,
} = require("../../usecases/file-category");

const makeGetFileCategory = require("./get-file-category");
const makePostFileCategory = require("./post-file-category");
const makePatchFileCategory = require("./patch-file-category");
const makeDeleteFileCategory = require("./delete-file-category");
const makeDataFileCategory = require("./data-file-category");
const makeGetListFileCategory = require("./get-list-file-category");

const getFileCategory = makeGetFileCategory({ singleFileCategory });
const postFileCategory = makePostFileCategory({ addFileCategory });
const patchFileCategory = makePatchFileCategory({ editFileCategory });
const deleteFileCategory = makeDeleteFileCategory({ removeFileCategory });
const dataFileCategory = makeDataFileCategory({ dataTableFileCategory });
const getListFileCategory = makeGetListFileCategory({ listFileCategory });

module.exports = Object.freeze({
  getFileCategory,
  dataFileCategory,
  postFileCategory,
  patchFileCategory,
  deleteFileCategory,
  getListFileCategory,
});
