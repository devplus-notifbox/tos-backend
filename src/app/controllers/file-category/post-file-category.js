module.exports = function makePostFileCategory({ addFileCategory }) {
  return async function postFileCategory(request) {
    try {
      if (!(await request.user.hasAccess("file-category", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = request.body;
      // console.log(data);
      const posted = await addFileCategory({
        data,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
