module.exports = function makePatchFileCategory({ editFileCategory }) {
  return async function patchFileCategory(request) {
    try {
      if (!(await request.user.hasAccess("file-category", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;

      const patched = await editFileCategory({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
