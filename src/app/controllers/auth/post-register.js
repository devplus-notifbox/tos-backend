module.exports = function makePostRegister({ register }) {
  return async function postRegister(request) {
    try {
      const data = request.body;
      const posted = await register({
        data,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
