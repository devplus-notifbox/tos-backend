module.exports = function makePostLoginWithPassword({ loginWithPassword }) {
  return async function postLoginWithPassword(request) {
    try {
      const data = request.body;
      const agent = request.useragent;
      const authData = await loginWithPassword({
        data,
        agent,
      });
      return {
        statusCode: 200,
        body: authData,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
