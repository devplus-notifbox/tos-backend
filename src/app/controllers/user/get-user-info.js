module.exports = function makeGetUserInfo({ singleUser }) {
  return async function getUserInfo(request) {
    try {
      const data = await singleUser({
        id: request.user.id,
      });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
