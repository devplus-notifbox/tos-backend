module.exports = function makePatchProfile({ editUser }) {
  return async function patchProfile(request) {
    try {
      const changes = request.body;

      const patched = await editUser({
        id: request.user.id,
        isAdmin: false,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
