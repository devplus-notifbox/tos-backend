module.exports = function makePostUser({ addUser }) {
  return async function postUser(request) {
    try {
      if (!(await request.user.hasAccess("user", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = request.body;

      const posted = await addUser({
        data,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
