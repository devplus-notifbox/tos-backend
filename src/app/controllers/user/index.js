const {
  singleUser,
  dataTableUser,
  addUser,
  editUser,
  removeUser,
  permission,
  listRole,
  switchZone,
  switchRole,
} = require("../../usecases/user");

const makeGetUser = require("./get-user");
const makePostUser = require("./post-user");
const makePatchUser = require("./patch-user");
const makeDeleteUser = require("./delete-user");
const makeDataUser = require("./data-user");
const makeGetPermission = require("./get-permission");
const makeGetListRole = require("./get-list-role");
const makePostSwitchZone = require("./post-switch-zone");
const makePostSwitchRole = require("./post-switch-role");
const makeGetUserInfo = require("./get-user-info");
const makePatchProfile = require("./patch-profile");

const getUser = makeGetUser({ singleUser });
const postUser = makePostUser({ addUser });
const patchUser = makePatchUser({ editUser });
const deleteUser = makeDeleteUser({ removeUser });
const dataUser = makeDataUser({ dataTableUser });
const getPermission = makeGetPermission({ permission });
const getListRole = makeGetListRole({ listRole });
const postSwitchZone = makePostSwitchZone({ switchZone });
const postSwitchRole = makePostSwitchRole({ switchRole });
const getUserInfo = makeGetUserInfo({ singleUser });
const patchProfile = makePatchProfile({ editUser });

module.exports = Object.freeze({
  getUser,
  dataUser,
  postUser,
  patchUser,
  deleteUser,
  getPermission,
  getListRole,
  postSwitchZone,
  postSwitchRole,
  getUserInfo,
  patchProfile,
});
