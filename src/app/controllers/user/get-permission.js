module.exports = function makeGetPermission({ permission }) {
  return async function getPermission(request) {
    try {
      const user = request.user;
      const data = await permission({ user });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
