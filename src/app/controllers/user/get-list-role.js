module.exports = function makeGetListRole({ listRole }) {
  return async function getListRole(request) {
    try {
      const user = request.user;
      const data = await listRole({ user });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
