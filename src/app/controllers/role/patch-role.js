module.exports = function makePatchRole({ editRole }) {
  return async function patchRole(request) {
    try {
      if (!(await request.user.hasAccess("role", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;

      const patched = await editRole({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
