const { listRegion } = require("../../usecases/region");

const makePostListRegion = require("./post-list-region");

const postListRegion = makePostListRegion({ listRegion });

module.exports = Object.freeze({
  postListRegion,
});
