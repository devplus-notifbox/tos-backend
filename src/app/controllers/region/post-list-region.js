module.exports = function makePostListRegion({ listRegion }) {
  return async function postListRegion(request) {
    try {
      const data = request.body;
      // console.log(data);
      const list = await listRegion({
        search: data.search || "",
        isId: data.id || false,
      });
      return {
        statusCode: 200,
        body: list,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
