module.exports = function makePatchSdm({ editSdm }) {
  return async function patchSdm(request) {
    try {
      if (!(await request.user.hasAccess("sdm", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }
      const isAdmin = await request.user.hasAccess("sdm", "admin");
      const changes = request.body;

      if (!isAdmin) {
        changes.zone_id = request.user.active_zone;
      }

      const patched = await editSdm({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
