const {
  singleSdm,
  dataTableSdm,
  addSdm,
  editSdm,
  removeSdm,
} = require("../../usecases/sdm");

const makeGetSdm = require("./get-sdm");
const makePostSdm = require("./post-sdm");
const makePatchSdm = require("./patch-sdm");
const makeDeleteSdm = require("./delete-sdm");
const makeDataSdm = require("./data-sdm");

const getSdm = makeGetSdm({ singleSdm });
const postSdm = makePostSdm({ addSdm });
const patchSdm = makePatchSdm({ editSdm });
const deleteSdm = makeDeleteSdm({ removeSdm });
const dataSdm = makeDataSdm({ dataTableSdm });

module.exports = Object.freeze({
  getSdm,
  dataSdm,
  postSdm,
  patchSdm,
  deleteSdm,
});
