const config = require("../../../config/app");
module.exports = function makeGetDefault() {
  return async function getDefault(request) {
    try {
      return {
        statusCode: 200,
        body: {
          max_file_size: config.maxDefaultSize,
          allowed_extension: config.allowedFileType,
        },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
