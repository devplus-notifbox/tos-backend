module.exports = function makeDataFile({ dataTableFile }) {
  return async function dataFile(request) {
    try {
      if (!(await request.user.hasAccess("file", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableFile({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
