module.exports = function makeLoadFile({ getFile }) {
  return async function loadFile(request) {
    try {
      const fileIds = request.body.fileId;

      const data = await getFile({
        fileIds,
      });
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
