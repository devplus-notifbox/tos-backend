module.exports = function makeDeleteFile({ removeFile }) {
  return async function deleteFile(request) {
    try {
      const id = request.query.id;

      const deleted = await removeFile({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
