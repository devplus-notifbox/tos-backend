const {
  singlePob,
  dataTablePob,
  addPob,
  editPob,
  removePob,
} = require("../../usecases/pob");

const makeGetPob = require("./get-pob");
const makePostPob = require("./post-pob");
const makePatchPob = require("./patch-pob");
const makeDeletePob = require("./delete-pob");
const makeDataPob = require("./data-pob");

const getPob = makeGetPob({ singlePob });
const postPob = makePostPob({ addPob });
const patchPob = makePatchPob({ editPob });
const deletePob = makeDeletePob({ removePob });
const dataPob = makeDataPob({ dataTablePob });

module.exports = Object.freeze({
  getPob,
  dataPob,
  postPob,
  patchPob,
  deletePob,
});
