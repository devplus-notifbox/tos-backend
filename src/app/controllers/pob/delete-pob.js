module.exports = function makeDeletePob({ removePob }) {
  return async function deletePob(request) {
    try {
      if (!(await request.user.hasAccess("pob", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removePob({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
