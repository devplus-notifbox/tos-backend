const {
  singleFasilitas,
  dataTableFasilitas,
  addFasilitas,
  editFasilitas,
  removeFasilitas,
} = require("../../usecases/fasilitas");

const makeGetFasilitas = require("./get-fasilitas");
const makePostFasilitas = require("./post-fasilitas");
const makePatchFasilitas = require("./patch-fasilitas");
const makeDeleteFasilitas = require("./delete-fasilitas");
const makeDataFasilitas = require("./data-fasilitas");

const getFasilitas = makeGetFasilitas({ singleFasilitas });
const postFasilitas = makePostFasilitas({ addFasilitas });
const patchFasilitas = makePatchFasilitas({ editFasilitas });
const deleteFasilitas = makeDeleteFasilitas({ removeFasilitas });
const dataFasilitas = makeDataFasilitas({ dataTableFasilitas });

module.exports = Object.freeze({
  getFasilitas,
  dataFasilitas,
  postFasilitas,
  patchFasilitas,
  deleteFasilitas,
});
