module.exports = function makePatchFasilitas({ editFasilitas }) {
  return async function patchFasilitas(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const changes = request.body;

      const patched = await editFasilitas({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
