module.exports = function makeDeleteFasilitas({ removeFasilitas }) {
  return async function deleteFasilitas(request) {
    try {
      if (!(await request.user.hasAccess("terminal", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeFasilitas({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
