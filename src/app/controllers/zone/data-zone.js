module.exports = function makeDataZone({ dataTableZone }) {
  return async function dataZone(request) {
    try {
      // if (!(await request.user.hasAccess("zone", "view"))) {
      //   throw new Error("You don't have permission to access this module!");
      // }

      const data = await dataTableZone({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
