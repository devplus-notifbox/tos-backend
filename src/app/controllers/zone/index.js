const {
  singleZone,
  dataTableZone,
  addZone,
  editZone,
  removeZone,
} = require("../../usecases/zone");

const makeGetZone = require("./get-zone");
const makePostZone = require("./post-zone");
const makePatchZone = require("./patch-zone");
const makeDeleteZone = require("./delete-zone");
const makeDataZone = require("./data-zone");
const makeGetZoneLevel = require("./get-zone-level");

const getZone = makeGetZone({ singleZone });
const postZone = makePostZone({ addZone });
const patchZone = makePatchZone({ editZone });
const deleteZone = makeDeleteZone({ removeZone });
const dataZone = makeDataZone({ dataTableZone });
const getZoneLevel = makeGetZoneLevel();

module.exports = Object.freeze({
  getZone,
  dataZone,
  postZone,
  patchZone,
  deleteZone,
  getZoneLevel,
});
