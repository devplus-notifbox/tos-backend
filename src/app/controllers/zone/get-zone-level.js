module.exports = function makeGetZoneLevel() {
  return async function getZone(request) {
    try {
      const data = [
        { value: "kementrian", text: "Kementrian" },
        { value: "bptd", text: "BPTD" },
        { value: "terminal", text: "Terminal" },
      ];
      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
