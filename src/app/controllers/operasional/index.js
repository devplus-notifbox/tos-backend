const {
  singleOperasional,
  dataTableOperasional,
  addOperasional,
  editOperasional,
  removeOperasional,
  uploadOperasional,
  dataTablePenumpangBerangkat,
} = require("../../usecases/operasional");

const makeGetOperasional = require("./get-operasional");
const makePostOperasional = require("./post-operasional");
const makePatchOperasional = require("./patch-operasional");
const makeDeleteOperasional = require("./delete-operasional");
const makeDataOperasional = require("./data-operasional");
const makePostUploadOperasional = require("./post-upload-operasional");
const makeDataPenumpangBerangkat = require("./data-penumpang-berangkat");

const getOperasional = makeGetOperasional({ singleOperasional });
const postOperasional = makePostOperasional({ addOperasional });
const patchOperasional = makePatchOperasional({ editOperasional });
const deleteOperasional = makeDeleteOperasional({ removeOperasional });
const dataOperasional = makeDataOperasional({ dataTableOperasional });
const postUploadOperasional = makePostUploadOperasional({ uploadOperasional });
const dataPenumpangBerangkat = makeDataPenumpangBerangkat({
  dataTablePenumpangBerangkat,
});

module.exports = Object.freeze({
  getOperasional,
  dataOperasional,
  postOperasional,
  patchOperasional,
  deleteOperasional,
  postUploadOperasional,
  dataPenumpangBerangkat,
});
