module.exports = function makeDataPenumpangBerangkat({
  dataTablePenumpangBerangkat,
}) {
  return async function dataPenumpangBerangkat(request) {
    try {
      if (!(await request.user.hasAccess("operasional", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTablePenumpangBerangkat({
        params: request.body,
        zoneId: request.user.active_zone,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
