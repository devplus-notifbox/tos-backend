module.exports = function makeDeleteOperasional({ removeOperasional }) {
  return async function deleteOperasional(request) {
    try {
      if (!(await request.user.hasAccess("operasional", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeOperasional({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
