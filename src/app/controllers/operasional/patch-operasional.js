module.exports = function makePatchOperasional({ editOperasional }) {
  return async function patchOperasional(request) {
    try {
      if (!(await request.user.hasAccess("operasional", "update"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const isAdmin = await request.user.hasAccess("operasional", "admin");
      const changes = request.body;

      if (!isAdmin) {
        changes.zone_id = request.user.active_zone;
      }

      const patched = await editOperasional({
        id: request.query.id,
        ...changes,
      });
      return {
        statusCode: 200,
        body: patched,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
