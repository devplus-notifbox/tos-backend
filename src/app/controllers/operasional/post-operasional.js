module.exports = function makePostOperasional({ addOperasional }) {
  return async function postOperasional(request) {
    try {
      if (!(await request.user.hasAccess("operasional", "create"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const isAdmin = await request.user.hasAccess("operasional", "admin");
      const data = request.body;

      if (!isAdmin) {
        data.zone_id = request.user.active_zone;
      }
      // console.log(data);
      const posted = await addOperasional({
        data,
        userId: request.user.id,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
