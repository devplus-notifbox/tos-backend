const mimeType = require("../file/mimetype.json");

module.exports = function makePostUploadOperasional({ uploadOperasional }) {
  return async function postUploadOperasional(req, res, next) {
    try {
      const data = req.body;
      // console.log(req);
      const posted = await uploadOperasional({
        req,
        res,
        mimeType,
      });
      return {
        statusCode: 201,
        body: posted,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
