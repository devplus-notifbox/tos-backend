module.exports = function makeDataOperasional({ dataTableOperasional }) {
  return async function dataOperasional(request) {
    try {
      if (!(await request.user.hasAccess("operasional", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableOperasional({
        params: request.body,
        isAdmin: await request.user.hasAccess("operasional", "admin"),
        zoneId: request.user.active_zone,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
