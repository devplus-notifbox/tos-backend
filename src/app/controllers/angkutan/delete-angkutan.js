module.exports = function makeDeleteAngkutan({ removeAngkutan }) {
  return async function deleteAngkutan(request) {
    try {
      if (!(await request.user.hasAccess("angkutan", "delete"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const id = request.query.id;

      const deleted = await removeAngkutan({ id });

      return {
        statusCode: 201,
        body: { deleted: deleted.count },
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
