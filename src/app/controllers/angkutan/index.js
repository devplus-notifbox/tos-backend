const {
  singleAngkutan,
  dataTableAngkutan,
  addAngkutan,
  editAngkutan,
  removeAngkutan,
} = require("../../usecases/angkutan");

const makeGetAngkutan = require("./get-angkutan");
const makePostAngkutan = require("./post-angkutan");
const makePatchAngkutan = require("./patch-angkutan");
const makeDeleteAngkutan = require("./delete-angkutan");
const makeDataAngkutan = require("./data-angkutan");

const getAngkutan = makeGetAngkutan({ singleAngkutan });
const postAngkutan = makePostAngkutan({ addAngkutan });
const patchAngkutan = makePatchAngkutan({ editAngkutan });
const deleteAngkutan = makeDeleteAngkutan({ removeAngkutan });
const dataAngkutan = makeDataAngkutan({ dataTableAngkutan });

module.exports = Object.freeze({
  getAngkutan,
  dataAngkutan,
  postAngkutan,
  patchAngkutan,
  deleteAngkutan,
});
