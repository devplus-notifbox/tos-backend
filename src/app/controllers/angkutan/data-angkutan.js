module.exports = function makeDataAngkutan({ dataTableAngkutan }) {
  return async function dataAngkutan(request) {
    try {
      if (!(await request.user.hasAccess("angkutan", "view"))) {
        throw new Error("You don't have permission to access this module!");
      }

      const data = await dataTableAngkutan({
        params: request.body,
      });

      return {
        statusCode: 200,
        body: data,
      };
    } catch (e) {
      // TODO: Error logging
      console.log(e);

      return {
        headers,
        statusCode: 400,
        body: {
          error: e.message,
        },
      };
    }
  };
};
