const { mAngkutan, mTrayek, mPob, mZone } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleAngkutan = require("./single-angkutan");
const makeDataTableAngkutan = require("./datatable-angkutan");
const makeAddAngkutan = require("./add-angkutan");
const makeEditAngkutan = require("./edit-angkutan");
const makeRemoveAngkutan = require("./remove-angkutan");

const singleAngkutan = makeSingleAngkutan({ mAngkutan });
const dataTableAngkutan = makeDataTableAngkutan({
  datatable,
  mAngkutan,
  mTrayek,
  mPob,
  mZone,
});
const addAngkutan = makeAddAngkutan({ mAngkutan });
const editAngkutan = makeEditAngkutan({ mAngkutan });
const removeAngkutan = makeRemoveAngkutan({ mAngkutan });

module.exports = Object.freeze({
  singleAngkutan,
  dataTableAngkutan,
  addAngkutan,
  editAngkutan,
  removeAngkutan,
});
