const { makeAngkutan } = require("../../entities");

module.exports = function makeEditAngkutan({ mAngkutan }) {
  return async function editAngkutan({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mAngkutan.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find angkutan with id " + id + ".");
    }

    const angkutan = await makeAngkutan({
      ...existing,
      ...changes,
    });

    await mAngkutan.update(
      {
        trayek_id: angkutan.getTrayekId(),
        pob_id: angkutan.getPobId(),
        tipe: angkutan.getTipe(),
        masa_berlaku: angkutan.getMasaBerlaku(),
        no_kendaraan: angkutan.getNoKendaraan(),
        no_uji: angkutan.getNoUji(),
        tgl_kadaluarsa_uji: angkutan.getTglKadaluarsaUji(),
        no_kps: angkutan.getNoKps(),
        tgl_kadaluarsa_kps: angkutan.getTglKadaluarsaKps(),
        no_srut: angkutan.getNoSrut(),
        no_rangka: angkutan.getNoRangka(),
        no_mesin: angkutan.getNoMesin(),
        merek: angkutan.getMerek(),
        jenis_kendaraan: angkutan.getJenisKendaraan(),
        tahun_buat: angkutan.getTahunBuat(),
        kapasitas: angkutan.getKapasitas(),
        deskripsi: angkutan.getDeskripsi(),
        aktif: angkutan.getAktif(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
