module.exports = function makeSingleAngkutan({ mAngkutan }) {
  return async function singleAngkutan({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const action = await mAngkutan.findByPk(id, { raw: true });

    if (!action) {
      throw new Error("Cannot find action with id " + id + ".");
    }

    return action;
  };
};
