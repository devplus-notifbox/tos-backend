const { makeAngkutan } = require("../../entities");

module.exports = function makeAddAngkutan({ mAngkutan }) {
  return async function addAngkutan({ data }) {
    const angkutan = await makeAngkutan(data);

    const insertAngkutan = await new mAngkutan({
      trayek_id: angkutan.getTrayekId(),
      pob_id: angkutan.getPobId(),
      tipe: angkutan.getTipe(),
      masa_berlaku: angkutan.getMasaBerlaku(),
      no_kendaraan: angkutan.getNoKendaraan(),
      no_uji: angkutan.getNoUji(),
      tgl_kadaluarsa_uji: angkutan.getTglKadaluarsaUji(),
      no_kps: angkutan.getNoKps(),
      tgl_kadaluarsa_kps: angkutan.getTglKadaluarsaKps(),
      no_srut: angkutan.getNoSrut(),
      no_rangka: angkutan.getNoRangka(),
      no_mesin: angkutan.getNoMesin(),
      merek: angkutan.getMerek(),
      jenis_kendaraan: angkutan.getJenisKendaraan(),
      tahun_buat: angkutan.getTahunBuat(),
      kapasitas: angkutan.getKapasitas(),
      deskripsi: angkutan.getDeskripsi(),
      aktif: angkutan.getAktif(),
    }).save();

    return {
      id: insertAngkutan.id,
    };
  };
};
