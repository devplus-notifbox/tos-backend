const { map, find } = require("lodash");
const { Op } = require("sequelize");
const { listJenisKendaraan } = require("../../../config/const");
const dataRole = require("../../controllers/role/data-role");
module.exports = function makeDataTableAngkutan({
  datatable,
  mAngkutan,
  mTrayek,
  mPob,
  mZone,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "nama_trayek",
          model: "trayek",
          field: "nama",
        },
        {
          column: "terminal",
          model: "trayek",
        },
        {
          column: "nama_pob",
          model: "pob",
          field: "nama",
        },
        {
          column: "id_pob",
          model: "pob",
          field: "kode",
        },
        {
          column: "tipe",
          model: "trayek",
        },
      ],
    });

    var data = await mAngkutan.findAndCountAll({
      ...dataTableObj,
      include: [
        {
          model: mTrayek,
          attributes: [],
        },
        {
          model: mPob,
          attributes: [],
        },
      ],
    });

    await Promise.all(
      map(data.rows, async (val) => {
        const data = val.dataValues;

        if (data.jenis_kendaraan) {
          const jenisKendaraan = find(listJenisKendaraan, {
            id: data.jenis_kendaraan,
          });
          data.jenis_kendaraan = jenisKendaraan ? jenisKendaraan.nama : null;
        }

        if (data.terminal && data.terminal.length > 0) {
          const termnaldata = await mZone.findAll({
            attributes: ["name"],
            where: { id: { [Op.in]: data.terminal } },
            raw: true,
          });
          data.terminal = map(termnaldata, (vval) => vval.name);
        }

        return data;
      })
    );

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
