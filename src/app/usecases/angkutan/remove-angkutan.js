module.exports = function makeRemoveAngkutan({ mAngkutan }) {
  return async function removeAngkutan({ id }) {
    const removeAngkutan = await mAngkutan.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeAngkutan,
    };
  };
};
