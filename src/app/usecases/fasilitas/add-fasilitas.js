const { makeFasilitas } = require("../../entities");

module.exports = function makeAddFasilitas({ mFasilitas }) {
  return async function addFasilitas({ data }) {
    const fasilitas = await makeFasilitas(data);

    const insertFasilitas = await new mFasilitas({
      zone_id: fasilitas.getZoneId(),
      nama: fasilitas.getNama(),
      deskripsi: fasilitas.getDeskripsi(),
      kategori: fasilitas.getKategori(),
      gambar: fasilitas.getGambar(),
      aktif: fasilitas.getAktif(),
    }).save();

    return {
      id: insertFasilitas.id,
    };
  };
};
