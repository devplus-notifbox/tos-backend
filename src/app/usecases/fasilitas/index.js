const { mFasilitas } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleFasilitas = require("./single-fasilitas");
const makeDataTableFasilitas = require("./datatable-fasilitas");
const makeAddFasilitas = require("./add-fasilitas");
const makeEditFasilitas = require("./edit-fasilitas");
const makeRemoveFasilitas = require("./remove-fasilitas");

const singleFasilitas = makeSingleFasilitas({ mFasilitas });
const dataTableFasilitas = makeDataTableFasilitas({ datatable, mFasilitas });
const addFasilitas = makeAddFasilitas({ mFasilitas });
const editFasilitas = makeEditFasilitas({ mFasilitas });
const removeFasilitas = makeRemoveFasilitas({ mFasilitas });

module.exports = Object.freeze({
  singleFasilitas,
  dataTableFasilitas,
  addFasilitas,
  editFasilitas,
  removeFasilitas,
});
