const { getFileOfData } = require("../file");

module.exports = function makeDataTableFasilitas({ datatable, mFasilitas }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mFasilitas.findAndCountAll(dataTableObj);

    data.rows = await getFileOfData({ data: data.rows, documents: "gambar" });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
