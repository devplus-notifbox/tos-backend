module.exports = function makeRemoveFasilitas({ mFasilitas }) {
  return async function removeFasilitas({ id }) {
    const removeFasilitas = await mFasilitas.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeFasilitas,
    };
  };
};
