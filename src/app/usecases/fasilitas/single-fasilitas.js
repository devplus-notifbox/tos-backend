module.exports = function makeSingleFasilitas({ mFasilitas }) {
  return async function singleFasilitas({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const fasilitas = await mFasilitas.findByPk(id, { raw: true });

    if (!fasilitas) {
      throw new Error("Cannot find fasilitas with id " + id + ".");
    }

    return fasilitas;
  };
};
