const { makeFasilitas } = require("../../entities");

module.exports = function makeEditFasilitas({ mFasilitas }) {
  return async function editFasilitas({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mFasilitas.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find fasilitas with id " + id + ".");
    }

    const fasilitas = await makeFasilitas({
      ...existing,
      ...changes,
    });

    await mFasilitas.update(
      {
        zone_id: fasilitas.getZoneId(),
        nama: fasilitas.getNama(),
        deskripsi: fasilitas.getDeskripsi(),
        kategori: fasilitas.getKategori(),
        gambar: fasilitas.getGambar(),
        aktif: fasilitas.getAktif(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
