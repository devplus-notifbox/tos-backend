module.exports = function makeSwitchRole({ mUserRole, mRole, mUser }) {
  return async function switchRole({ data, user }) {
    const role = await mRole.findByPk(data.role_id);

    if (role && user.active_zone) {
      const userRole = await mUserRole.findOne({
        attributes: ["roles"],
        where: {
          user_id: user.id,
          zone_id: user.active_zone,
        },
      });

      if (userRole && userRole.roles.indexOf(role.slug) !== -1) {
        await mUser.update(
          { active_role: role.id, updated_by: user.id },
          { where: { id: user.id } }
        );

        return { to: role.default_route };
      } else {
        throw new Error(
          "Invalid Role or Role is not registered in the current zone"
        );
      }
    }

    return null;
  };
};
