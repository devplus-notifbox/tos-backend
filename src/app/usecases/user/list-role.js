const Sequelize = require("sequelize");
const _ = require("lodash");

module.exports = function makeListRole({ mUserRole, mRole, mZone }) {
  return async function listRole({ user } = {}) {
    if (!user) {
      throw new Error("Not Authenticated");
    }

    const userRole = await mUserRole.findAll({
      attributes: [
        "zone_id",
        [Sequelize.col("zone.name"), "zone_name"],
        "roles",
      ],
      where: {
        user_id: user.id,
      },
      include: [
        {
          model: mZone,
          attributes: [],
        },
      ],
    });

    const listRole = await mRole.findAll({
      attributes: ["id", "slug", "name"],
    });

    const listUserRole = [];
    _.map(userRole, (value) => {
      if (value.roles) {
        const objRole = [];
        _.map(value.roles, (vrole) => {
          const findRole = _.find(listRole, { slug: vrole });
          if (findRole) {
            objRole.push(findRole);
          }
        });
        value.roles = objRole;
        listUserRole.push(value);
      }
    });

    return listUserRole;
  };
};
