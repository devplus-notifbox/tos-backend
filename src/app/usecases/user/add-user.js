const { makeUser } = require("../../entities");

module.exports = function makeAddUser({ mUser }) {
  return async function addUser({ data }) {
    const user = await makeUser(data);

    const insertUser = await new mUser({
      nik: user.getNik(),
      name: user.getName(),
      email: user.getEmail(),
      password: user.getPassword(),
      username: user.getUsername(),
      phone: user.getPhone(),
      address: user.getAddress(),
      active: user.getActive(),
    }).save();

    return {
      id: insertUser.id,
    };
  };
};
