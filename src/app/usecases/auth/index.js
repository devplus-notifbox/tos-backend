const { mUser, mUserLog, mZone, mRole, mUserRole } = require("../../models");

const makeLoginWithPassword = require("./login-with-password");
const makeRefreshToken = require("./refresh-token");
const makeRegister = require("./register");

const loginWithPassword = makeLoginWithPassword({
  mUser,
  mUserLog,
  mZone,
  mRole,
});

const refreshToken = makeRefreshToken({ mUser, mUserRole, mZone, mRole });
const register = makeRegister({ mUser });

module.exports = Object.freeze({
  loginWithPassword,
  refreshToken,
  register,
});
