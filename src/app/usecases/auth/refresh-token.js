const token = require("../../../utils/token");

module.exports = function makeRefreshToken({ mUser, mUserRole, mZone, mRole }) {
  return async function refreshToken({ data }) {
    const verify = new token("refresh").verify(data.token);

    if (verify) {
      const user = await mUser.findByPk(verify.userId);
      if (!user) {
        throw new Error("User not found.");
      }
      if (!user.active) {
        throw new Error("User is not active.");
      }

      // Validate Role & Zone
      const userRole = await mUserRole.findOne({
        attributes: ["id", "roles"],
        where: {
          user_id: user.id,
          zone_id: user.active_zone,
        },
      });

      const role = await mRole.findByPk(user.active_role, {
        attributes: ["id", "slug", "name"],
        raw: true,
      });
      if (!role) {
        throw new Error("Invalid Role.");
      }

      if (userRole) {
        if (userRole.roles.indexOf(role.slug) == -1) {
          throw new Error("Invalid Role.");
        }
      } else {
        throw new Error("Invalid Zone.");
      }

      const zone = await mZone.findByPk(user.active_zone, {
        attributes: ["id", "code", "name", "level"],
        raw: true,
      });

      const accesToken = new token("password").generate({
        userId: verify.userId,
        grantType: "password",
      });
      const refreshToken = new token("refresh").generate({
        userId: verify.userId,
        grantType: "password",
      });

      return {
        token_type: "bearer",
        access_token: accesToken,
        refresh_token: refreshToken,
        user: {
          id: user.id,
          email: user.email,
          name: user.name,
          username: user.username,
          activeZone: zone,
          activeRole: role,
        },
        expires_in: process.env.JWT_EXPIRES_IN,
      };
    } else {
      throw new Error("Invalid Token");
    }
  };
};
