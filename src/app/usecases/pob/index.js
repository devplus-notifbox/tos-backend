const { mPob } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSinglePob = require("./single-pob");
const makeDataTablePob = require("./datatable-pob");
const makeAddPob = require("./add-pob");
const makeEditPob = require("./edit-pob");
const makeRemovePob = require("./remove-pob");

const singlePob = makeSinglePob({ mPob });
const dataTablePob = makeDataTablePob({ datatable, mPob });
const addPob = makeAddPob({ mPob });
const editPob = makeEditPob({ mPob });
const removePob = makeRemovePob({ mPob });

module.exports = Object.freeze({
  singlePob,
  dataTablePob,
  addPob,
  editPob,
  removePob,
});
