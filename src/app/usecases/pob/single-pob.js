module.exports = function makeSinglePob({ mPob }) {
  return async function singlePob({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const pob = await mPob.findByPk(id, { raw: true });

    if (!pob) {
      throw new Error("Cannot find pob with id " + id + ".");
    }

    return pob;
  };
};
