module.exports = function makeDataTablePob({ datatable, mPob }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mPob.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
