module.exports = function makeRemovePob({ mPob }) {
  return async function removePob({ id }) {
    const removePob = await mPob.destroy({
      where: {
        id,
      },
    });

    return {
      count: removePob,
    };
  };
};
