const { makePob } = require("../../entities");

module.exports = function makeEditPob({ mPob }) {
  return async function editPob({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mPob.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find pob with id " + id + ".");
    }

    const pob = await makePob({
      ...existing,
      ...changes,
    });

    await mPob.update(
      {
        kode: pob.getKode(),
        nama: pob.getNama(),
        nama_direktur: pob.getNamaDirektur(),
        no_izin: pob.getNoIzin(),
        tgl_kadaluarsa_izin: pob.getTglKadaluarsaIzin(),
        jumlah_kendaraan: pob.getJumlahKendaraan(),
        telepon: pob.getTelepon(),
        kode_pos: pob.getKodePos(),
        village_id: pob.getVillageId(),
        alamat: pob.getAlamat(),
        tipe: pob.getTipe(),
        aktif: pob.getAktif(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
