const { makePob } = require("../../entities");

module.exports = function makeAddPob({ mPob }) {
  return async function addPob({ data }) {
    const pob = await makePob(data);

    const insertPob = await new mPob({
      kode: pob.getKode(),
      nama: pob.getNama(),
      nama_direktur: pob.getNamaDirektur(),
      no_izin: pob.getNoIzin(),
      tgl_kadaluarsa_izin: pob.getTglKadaluarsaIzin(),
      jumlah_kendaraan: pob.getJumlahKendaraan(),
      telepon: pob.getTelepon(),
      kode_pos: pob.getKodePos(),
      village_id: pob.getVillageId(),
      alamat: pob.getAlamat(),
      tipe: pob.getTipe(),
      aktif: pob.getAktif(),
    }).save();

    return {
      id: insertPob.id,
    };
  };
};
