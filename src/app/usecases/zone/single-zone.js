module.exports = function makeSingleZone({ mZone }) {
  return async function singleZone({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const zone = await mZone.findByPk(id, { raw: true });

    if (!zone) {
      throw new Error("Cannot find zone with id " + id + ".");
    }

    return zone;
  };
};
