const { mZone } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleZone = require("./single-zone");
const makeDataTableZone = require("./datatable-zone");
const makeAddZone = require("./add-zone");
const makeEditZone = require("./edit-zone");
const makeRemoveZone = require("./remove-zone");

const singleZone = makeSingleZone({ mZone });
const dataTableZone = makeDataTableZone({ datatable, mZone });
const addZone = makeAddZone({ mZone });
const editZone = makeEditZone({ mZone });
const removeZone = makeRemoveZone({ mZone });

module.exports = Object.freeze({
  singleZone,
  dataTableZone,
  addZone,
  editZone,
  removeZone,
});
