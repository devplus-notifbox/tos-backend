const { makeZone } = require("../../entities");

module.exports = function makeEditZone({ mZone }) {
  return async function editZone({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mZone.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find zone with id " + id + ".");
    }

    const zone = await makeZone({
      ...existing,
      ...changes,
    });

    await mZone.update(
      {
        code: zone.getCode(),
        name: zone.getName(),
        phone: zone.getPhone(),
        address: zone.getAddress(),
        lat: zone.getLat(),
        lng: zone.getLng(),
        level: zone.getLevel(),
        parent: zone.getParent(),
        active: zone.getActive(),
        village_id: zone.getVillageId(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
