module.exports = function makeDataTableZone({ datatable, mZone }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mZone.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
