const zone = require("../../models/zone");

module.exports = function makeSingleTerminal({ mTerminal }) {
  return async function singleTerminal({ zone_id } = {}) {
    if (!zone_id) {
      throw new Error("You must supply an id.");
    }

    const terminal = await mTerminal.findOne({
      where: { zone_id: zone_id },
      raw: true,
    });

    if (!terminal) {
      throw new Error("Cannot find terminal with id " + id + ".");
    }

    return terminal;
  };
};
