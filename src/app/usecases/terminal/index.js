const { mTerminal, mZone } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeDataTableTerminal = require("./datatable-terminal");
const makeAddTerminal = require("./add-terminal");
const makeEditTerminal = require("./edit-terminal");
const makeSingleTerminal = require("./single-terminal");

const dataTableTerminal = makeDataTableTerminal({
  datatable,
  mTerminal,
  mZone,
});
const addTerminal = makeAddTerminal({ mTerminal });
const editTerminal = makeEditTerminal({ mTerminal });
const singleTerminal = makeSingleTerminal({ mTerminal });

module.exports = Object.freeze({
  dataTableTerminal,
  addTerminal,
  editTerminal,
  singleTerminal,
});
