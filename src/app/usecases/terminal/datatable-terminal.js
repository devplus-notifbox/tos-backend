const { getFileOfData } = require("../file");

module.exports = function makeDataTableTerminal({
  datatable,
  mTerminal,
  mZone,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "gambar",
          model: "terminal",
        },
        {
          column: "tipe",
          model: "terminal",
        },
      ],
    });
    var data = await mZone.findAndCountAll({
      ...dataTableObj,
      where: {
        ...dataTableObj.where,
        level: "terminal",
      },
      include: [
        {
          model: mTerminal,
          attributes: [],
        },
      ],
      raw: true,
    });

    data.rows = await getFileOfData({ data: data.rows, documents: "gambar" });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
