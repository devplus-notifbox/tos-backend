const { makeTerminal } = require("../../entities");

module.exports = function makeEditTerminal({ mTerminal }) {
  return async function editTerminal({ zone_id, ...changes } = {}) {
    if (!zone_id) {
      throw new Error("You must supply an d.");
    }

    const existing = await mTerminal.findOne({
      where: { zone_id: zone_id },
      raw: true,
    });

    if (!existing) {
      throw new Error("Cannot find terminal with id " + zone_id + ".");
    }

    const terminal = await makeTerminal({
      ...existing,
      ...changes,
    });

    await mTerminal.update(
      {
        deskripsi: terminal.getDeskripsi(),
        gambar: terminal.getGambar(),
        luas_lahan: terminal.getLuasLahan(),
        luas_bangunan: terminal.getLuasBangunan(),
        luas_area_pengembangan: terminal.getLuasAreaPengembangan(),
        tipe: terminal.getTipe(),
        status_p3d: terminal.getStatusP3d(),
        pob: terminal.getPob(),
        sk_penetapan_lokasi: terminal.getSkPenetapanLokasi(),
        bast: terminal.getBast(),
        sertifikat_tanah: terminal.getSertifikatTanah(),
      },
      { where: { zone_id: zone_id } }
    );

    return {
      id: zone_id,
    };
  };
};
