const { makeTerminal } = require("../../entities");

module.exports = function makeAddTerminal({ mTerminal }) {
  return async function addTerminal({ data }) {
    const terminal = await makeTerminal(data);

    const insertTerminal = await new mTerminal({
      zone_id: terminal.getZoneId(),
      deskripsi: terminal.getDeskripsi(),
      gambar: terminal.getGambar(),
      luas_lahan: terminal.getLuasLahan(),
      luas_bangunan: terminal.getLuasBangunan(),
      luas_area_pengembangan: terminal.getLuasAreaPengembangan(),
      tipe: terminal.getTipe(),
      status_p3d: terminal.getStatusP3d(),
      pob: terminal.getPob(),
      sk_penetapan_lokasi: terminal.getSkPenetapanLokasi(),
      bast: terminal.getBast(),
      sertifikat_tanah: terminal.getSertifikatTanah(),
    }).save();

    return {
      id: insertTerminal.id,
    };
  };
};
