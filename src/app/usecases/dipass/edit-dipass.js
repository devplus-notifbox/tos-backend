const { makeDipass } = require("../../entities");

module.exports = function makeEditDipass({ mDipass }) {
  return async function editDipass({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mDipass.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find dipass with id " + id + ".");
    }

    const dipass = await makeDipass({
      ...existing,
      ...changes,
    });

    await mDipass.update(
      {
        nama: dipass.getNama(),
        nama_bus: dipass.getNamaBus(),
        no_kursi: dipass.getNoKursi(),
        waktu_berangkat: dipass.getWaktuBerangkat(),
        waktu_tiba: dipass.getWaktuTiba(),
        no_identitas: dipass.getNoIdentitas(),
        rute_asal: dipass.getRuteAsal(),
        rute_tujuan: dipass.getRuteTujuan(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
