module.exports = function makeRemoveDipass({ mDipass }) {
  return async function removeDipass({ id }) {
    const removeDipass = await mDipass.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeDipass,
    };
  };
};
