const { mDipass } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleDipass = require("./single-dipass");
const makeDataTableDipass = require("./datatable-dipass");
const makeAddDipass = require("./add-dipass");
const makeEditDipass = require("./edit-dipass");
const makeRemoveDipass = require("./remove-dipass");

const singleDipass = makeSingleDipass({ mDipass });
const dataTableDipass = makeDataTableDipass({ datatable, mDipass });
const addDipass = makeAddDipass({ mDipass });
const editDipass = makeEditDipass({ mDipass });
const removeDipass = makeRemoveDipass({ mDipass });

module.exports = Object.freeze({
  singleDipass,
  dataTableDipass,
  addDipass,
  editDipass,
  removeDipass,
});
