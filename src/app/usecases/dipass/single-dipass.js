module.exports = function makeSingleDipass({ mDipass }) {
  return async function singleDipass({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const dipass = await mDipass.findByPk(id, { raw: true });

    if (!dipass) {
      throw new Error("Cannot find dipass with id " + id + ".");
    }

    return dipass;
  };
};
