module.exports = function makeDataTableDipass({ datatable, mDipass }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mDipass.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
