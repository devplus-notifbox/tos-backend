const { makeDipass } = require("../../entities");

module.exports = function makeAddDipass({ mDipass }) {
  return async function addDipass({ data }) {
    const dipass = await makeDipass(data);

    const insertDipass = await new mDipass({
      nama: dipass.getNama(),
      nama_bus: dipass.getNamaBus(),
      no_kursi: dipass.getNoKursi(),
      waktu_berangkat: dipass.getWaktuBerangkat(),
      waktu_tiba: dipass.getWaktuTiba(),
      no_identitas: dipass.getNoIdentitas(),
      rute_asal: dipass.getRuteAsal(),
      rute_tujuan: dipass.getRuteTujuan(),
    }).save();

    return {
      id: insertDipass.id,
    };
  };
};
