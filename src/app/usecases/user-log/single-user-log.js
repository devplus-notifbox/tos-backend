module.exports = function makeSingleUserLog({ mUserLog }) {
  return async function singleUserLog({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const userLog = await mUserLog.findByPk(id, { raw: true });

    if (!userLog) {
      throw new Error("Cannot find user role with id " + id + ".");
    }

    return userLog;
  };
};
