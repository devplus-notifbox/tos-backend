module.exports = function makeDataTableRole({ datatable, mRole }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mRole.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
