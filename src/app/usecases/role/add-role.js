const { makeRole } = require("../../entities");

module.exports = function makeAddRole({ mRole }) {
  return async function addRole({ data }) {
    const role = await makeRole(data);

    const insertRole = await new mRole({
      slug: role.getSlug(),
      name: role.getName(),
      default_route: role.getDefaultRoute(),
      permissions: role.getPermissions(),
    }).save();

    return {
      id: insertRole.id,
    };
  };
};
