module.exports = function makeSingleRole({ mRole }) {
  return async function singleRole({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const role = await mRole.findByPk(id, { raw: true });

    if (!role) {
      throw new Error("Cannot find role with id " + id + ".");
    }

    return role;
  };
};
