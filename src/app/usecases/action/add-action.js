const { makeAction } = require("../../entities");

module.exports = function makeAddAction({ mAction }) {
  return async function addAction({ data }) {
    const action = await makeAction(data);

    const insertAction = await new mAction({
      slug: action.getSlug(),
      name: action.getName(),
    }).save();

    return {
      id: insertAction.id,
    };
  };
};
