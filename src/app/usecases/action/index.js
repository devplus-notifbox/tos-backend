const { mAction } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleAction = require("./single-action");
const makeDataTableAction = require("./datatable-action");
const makeAddAction = require("./add-action");
const makeEditAction = require("./edit-action");
const makeRemoveAction = require("./remove-action");
const makeListAction = require("./list-action");

const singleAction = makeSingleAction({ mAction });
const dataTableAction = makeDataTableAction({ datatable, mAction });
const addAction = makeAddAction({ mAction });
const editAction = makeEditAction({ mAction });
const removeAction = makeRemoveAction({ mAction });
const listAction = makeListAction({ mAction });

module.exports = Object.freeze({
  singleAction,
  dataTableAction,
  addAction,
  editAction,
  removeAction,
  listAction,
});
