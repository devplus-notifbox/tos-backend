module.exports = function makeRemoveAction({ mAction }) {
  return async function removeAction({ id }) {
    const removeAction = await mAction.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeAction,
    };
  };
};
