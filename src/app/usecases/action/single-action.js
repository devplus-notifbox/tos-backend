module.exports = function makeSingleAction({ mAction }) {
  return async function singleAction({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const action = await mAction.findByPk(id, { raw: true });

    if (!action) {
      throw new Error("Cannot find action with id " + id + ".");
    }

    return action;
  };
};
