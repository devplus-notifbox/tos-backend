module.exports = function makeListAction({ mAction }) {
  return async function listAction() {
    const action = await mAction.findAll({
      attributes: ["id", "slug", "name"],
      order: [["name", "ASC"]],
      raw: true,
    });

    return action;
  };
};
