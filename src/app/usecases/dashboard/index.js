const datatable = require("../../../utils/datatable");
const {
  mDashboardOperasionalKendaraan,
  mDashboardOperasionalPenumpang,
} = require("../../models");

const makeDashboardOperasionalKendaraan = require("./dashboard-operasional-kendaraan");
const makeDashboardOperasionalPenumpang = require("./dashboard-operasional-penumpang");

const dashboardOperasionalKendaraan = makeDashboardOperasionalKendaraan({
  datatable,
  mDashboardOperasionalKendaraan,
});

const dashboardOperasionalPenumpang = makeDashboardOperasionalPenumpang({
  datatable,
  mDashboardOperasionalPenumpang,
});

module.exports = Object.freeze({
  dashboardOperasionalKendaraan,
  dashboardOperasionalPenumpang,
});
