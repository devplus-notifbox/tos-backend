const Sequelize = require("sequelize");
const moment = require("moment");
module.exports = function makeDashboardOperasionalKendaraan({
  datatable,
  mDashboardOperasionalPenumpang,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);

    var attrData = {
      ...dataTableObj,
      attributes: [
        "id",
        "name",
        [Sequelize.fn("DATE", Sequelize.col("tanggal")), "tanggal"],
        [Sequelize.fn("SUM", Sequelize.col("akap_naik")), "akap_naik"],
        [Sequelize.fn("SUM", Sequelize.col("akap_turun")), "akap_turun"],
        [Sequelize.fn("SUM", Sequelize.col("akap_tiba")), "akap_tiba"],
        [
          Sequelize.fn("SUM", Sequelize.col("akap_berangkat")),
          "akap_berangkat",
        ],
        [Sequelize.fn("SUM", Sequelize.col("akdp_naik")), "akdp_naik"],
        [Sequelize.fn("SUM", Sequelize.col("akdp_turun")), "akdp_turun"],
        [Sequelize.fn("SUM", Sequelize.col("akdp_tiba")), "akdp_tiba"],
        [
          Sequelize.fn("SUM", Sequelize.col("akdp_berangkat")),
          "akdp_berangkat",
        ],
      ],
      group: ["id", "name", Sequelize.fn("DATE", Sequelize.col("tanggal"))],
      order: [[Sequelize.fn("DATE", Sequelize.col("tanggal")), "asc"]],
    };
    if (params.date) {
      const from = params.date[0];
      const to = moment(params.date[1]).add(1, "d").format("YYYY-MM-DD");

      attrData.where = {
        [Sequelize.Op.and]: [
          {
            tanggal: {
              [Sequelize.Op.gte]: from,
            },
          },
          {
            tanggal: {
              [Sequelize.Op.lte]: to,
            },
          },
        ],
      };
    }

    var data = await mDashboardOperasionalPenumpang.findAndCountAll(attrData);

    return {
      recordsTotal: data.count.length,
      data: data.rows,
    };
  };
};
