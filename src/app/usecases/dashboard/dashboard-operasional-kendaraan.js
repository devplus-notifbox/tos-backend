const Sequelize = require("sequelize");
const moment = require("moment");
module.exports = function makeDashboardOperasionalKendaraan({
  datatable,
  mDashboardOperasionalKendaraan,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);

    var attrData = {
      ...dataTableObj,
      attributes: [
        "id",
        "name",
        [Sequelize.fn("DATE", Sequelize.col("tanggal")), "tanggal"],
        [Sequelize.fn("SUM", Sequelize.col("akap_in")), "akap_in"],
        [Sequelize.fn("SUM", Sequelize.col("akap_out")), "akap_out"],
        [Sequelize.fn("SUM", Sequelize.col("akdp_in")), "akdp_in"],
        [Sequelize.fn("SUM", Sequelize.col("akdp_out")), "akdp_out"],
      ],
      group: ["id", "name", Sequelize.fn("DATE", Sequelize.col("tanggal"))],
      order: [[Sequelize.fn("DATE", Sequelize.col("tanggal")), "asc"]],
    };
    if (params.date) {
      const from = params.date[0];
      const to = moment(params.date[1]).add(1, "d").format("YYYY-MM-DD");

      attrData.where = {
        [Sequelize.Op.and]: [
          {
            tanggal: {
              [Sequelize.Op.gte]: from,
            },
          },
          {
            tanggal: {
              [Sequelize.Op.lte]: to,
            },
          },
        ],
      };
    }

    var data = await mDashboardOperasionalKendaraan.findAndCountAll(attrData);

    return {
      recordsTotal: data.count.length,
      data: data.rows,
    };
  };
};
