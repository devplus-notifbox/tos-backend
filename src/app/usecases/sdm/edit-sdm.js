const { makeSdm } = require("../../entities");

module.exports = function makeEditSdm({ mSdm }) {
  return async function editSdm({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mSdm.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find sdm with id " + id + ".");
    }

    const sdm = await makeSdm({
      ...existing,
      ...changes,
    });

    await mSdm.update(
      {
        zone_id: sdm.getZoneId(),
        nip: sdm.getNip(),
        nama: sdm.getNama(),
        jabatan: sdm.getJabatan(),
        tipe: sdm.getTipe(),
        alamat: sdm.getAlamat(),
        kode_pos: sdm.getKodePos(),
        telepon: sdm.getTelepon(),
        village_id: sdm.getVillageId(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
