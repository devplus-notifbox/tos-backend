module.exports = function makeDataTableSdm({ datatable, mSdm }) {
  return async function ({ params, isAdmin = true, zoneId = null } = {}) {
    var dataTableObj = await datatable(params);
    if (!isAdmin) {
      dataTableObj.where = {
        ...dataTableObj.where,
        zone_id: zoneId,
      };
    }
    var data = await mSdm.findAndCountAll({
      ...dataTableObj,
    });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
