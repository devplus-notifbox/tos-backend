module.exports = function makeRemoveSdm({ mSdm }) {
  return async function removeSdm({ id }) {
    const removeSdm = await mSdm.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeSdm,
    };
  };
};
