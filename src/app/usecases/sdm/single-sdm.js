module.exports = function makeSingleSdm({ mSdm }) {
  return async function singleSdm({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const sdm = await mSdm.findByPk(id, { raw: true });

    if (!sdm) {
      throw new Error("Cannot find sdm with id " + id + ".");
    }

    return sdm;
  };
};
