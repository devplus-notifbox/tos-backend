const { makeSdm } = require("../../entities");

module.exports = function makeAddSdm({ mSdm }) {
  return async function addSdm({ data }) {
    const sdm = await makeSdm(data);

    const insertSdm = await new mSdm({
      zone_id: sdm.getZoneId(),
      nip: sdm.getNip(),
      nama: sdm.getNama(),
      jabatan: sdm.getJabatan(),
      tipe: sdm.getTipe(),
      alamat: sdm.getAlamat(),
      kode_pos: sdm.getKodePos(),
      telepon: sdm.getTelepon(),
      village_id: sdm.getVillageId(),
    }).save();

    return {
      id: insertSdm.id,
    };
  };
};
