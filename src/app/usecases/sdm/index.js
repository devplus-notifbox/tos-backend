const { mSdm } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleSdm = require("./single-sdm");
const makeDataTableSdm = require("./datatable-sdm");
const makeAddSdm = require("./add-sdm");
const makeEditSdm = require("./edit-sdm");
const makeRemoveSdm = require("./remove-sdm");

const singleSdm = makeSingleSdm({ mSdm });
const dataTableSdm = makeDataTableSdm({ datatable, mSdm });
const addSdm = makeAddSdm({ mSdm });
const editSdm = makeEditSdm({ mSdm });
const removeSdm = makeRemoveSdm({ mSdm });

module.exports = Object.freeze({
  singleSdm,
  dataTableSdm,
  addSdm,
  editSdm,
  removeSdm,
});
