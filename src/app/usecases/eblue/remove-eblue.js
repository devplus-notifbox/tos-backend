module.exports = function makeRemoveEblue({ mEblue }) {
  return async function removeEblue({ id }) {
    const removeEblue = await mEblue.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeEblue,
    };
  };
};
