module.exports = function makeDataTableEblue({ datatable, mEblue }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mEblue.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
