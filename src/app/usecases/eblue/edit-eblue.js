const { makeEblue } = require("../../entities");

module.exports = function makeEditEblue({ mEblue }) {
  return async function editEblue({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mEblue.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find eblue with id " + id + ".");
    }

    const eblue = await makeEblue({
      ...existing,
      ...changes,
    });

    await mEblue.update(
      {
        nama_pemilik: eblue.getNamaPemilik(),
        alamat_pemilik: eblue.getAlamatPemilik(),
        no_registrasi_kendaraan: eblue.getNoRegistrasiKendaraan(),
        no_rangka: eblue.getNoRangka(),
        no_mesin: eblue.getNoMesin(),
        jenis_kendaraan: eblue.getJenisKendaraan(),
        merk: eblue.getMerk(),
        tipe: eblue.getTipe(),
        keterangan_hasil_uji: eblue.getKeteranganHasilUji(),
        masa_berlaku: eblue.getMasaBerlaku(),
        petugas_penguji: eblue.getPetugasPenguji(),
        nrp_petugas_penguji: eblue.getNrpPetugasPenguji(),
        unit_pelaksana_teknis: eblue.getUnitPelaksanaTeknis(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
