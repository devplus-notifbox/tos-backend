const { mEblue } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleEblue = require("./single-eblue");
const makeDataTableEblue = require("./datatable-eblue");
const makeAddEblue = require("./add-eblue");
const makeEditEblue = require("./edit-eblue");
const makeRemoveEblue = require("./remove-eblue");

const singleEblue = makeSingleEblue({ mEblue });
const dataTableEblue = makeDataTableEblue({ datatable, mEblue });
const addEblue = makeAddEblue({ mEblue });
const editEblue = makeEditEblue({ mEblue });
const removeEblue = makeRemoveEblue({ mEblue });

module.exports = Object.freeze({
  singleEblue,
  dataTableEblue,
  addEblue,
  editEblue,
  removeEblue,
});
