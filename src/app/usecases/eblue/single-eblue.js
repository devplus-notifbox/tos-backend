module.exports = function makeSingleEblue({ mEblue }) {
  return async function singleEblue({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const eblue = await mEblue.findByPk(id, { raw: true });

    if (!eblue) {
      throw new Error("Cannot find eblue with id " + id + ".");
    }

    return eblue;
  };
};
