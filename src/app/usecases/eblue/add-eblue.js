const { makeEblue } = require("../../entities");

module.exports = function makeAddEblue({ mEblue }) {
  return async function addEblue({ data }) {
    const eblue = await makeEblue(data);

    const insertEblue = await new mEblue({
      nama_pemilik: eblue.getNamaPemilik(),
      alamat_pemilik: eblue.getAlamatPemilik(),
      no_registrasi_kendaraan: eblue.getNoRegistrasiKendaraan(),
      no_rangka: eblue.getNoRangka(),
      no_mesin: eblue.getNoMesin(),
      jenis_kendaraan: eblue.getJenisKendaraan(),
      merk: eblue.getMerk(),
      tipe: eblue.getTipe(),
      keterangan_hasil_uji: eblue.getKeteranganHasilUji(),
      masa_berlaku: eblue.getMasaBerlaku(),
      petugas_penguji: eblue.getPetugasPenguji(),
      nrp_petugas_penguji: eblue.getNrpPetugasPenguji(),
      unit_pelaksana_teknis: eblue.getUnitPelaksanaTeknis(),
    }).save();

    return {
      id: insertEblue.id,
    };
  };
};
