module.exports = function makeRemoveSpinoam({ mSpinoam }) {
  return async function removeSpinoam({ id }) {
    const removeSpinoam = await mSpinoam.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeSpinoam,
    };
  };
};
