const { makeSpinoam } = require("../../entities");

module.exports = function makeEditSpinoam({ mSpinoam }) {
  return async function editSpinoam({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mSpinoam.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find spinoam with id " + id + ".");
    }

    const spinoam = await makeSpinoam({
      ...existing,
      ...changes,
    });

    await mSpinoam.update(
      {
        perusahaan_id: spinoam.getPerusahaanId(),
        jenis_pelayanan: spinoam.getJenisPelayanan(),
        nama_perusahaan: spinoam.getNamaPerusahaan(),
        alamat: spinoam.getAlamat(),
        no_sk: spinoam.getNoSk(),
        tgl_exp_sk: spinoam.getTglExpSk(),
        no_uji: spinoam.getNoUji(),
        tgl_exp_uji: spinoam.getTglExpUji(),
        no_kps: spinoam.getNoKps(),
        tgl_exp_kps: spinoam.getTglExpKps(),
        no_rangka: spinoam.getNoRangka(),
        no_mesin: spinoam.getNoMesin(),
        merek: spinoam.getMerek(),
        tahun: spinoam.getTahun(),
        no_kendaraan: spinoam.getNoKendaraan(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
