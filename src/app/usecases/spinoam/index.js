const { mSpinoam } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleSpinoam = require("./single-spinoam");
const makeDataTableSpinoam = require("./datatable-spinoam");
const makeAddSpinoam = require("./add-spinoam");
const makeEditSpinoam = require("./edit-spinoam");
const makeRemoveSpinoam = require("./remove-spinoam");

const singleSpinoam = makeSingleSpinoam({ mSpinoam });
const dataTableSpinoam = makeDataTableSpinoam({ datatable, mSpinoam });
const addSpinoam = makeAddSpinoam({ mSpinoam });
const editSpinoam = makeEditSpinoam({ mSpinoam });
const removeSpinoam = makeRemoveSpinoam({ mSpinoam });

module.exports = Object.freeze({
  singleSpinoam,
  dataTableSpinoam,
  addSpinoam,
  editSpinoam,
  removeSpinoam,
});
