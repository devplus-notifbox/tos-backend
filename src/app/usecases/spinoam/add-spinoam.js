const { makeSpinoam } = require("../../entities");

module.exports = function makeAddSpinoam({ mSpinoam }) {
  return async function addSpinoam({ data }) {
    const spinoam = await makeSpinoam(data);

    const insertSpinoam = await new mSpinoam({
      perusahaan_id: spinoam.getPerusahaanId(),
      jenis_pelayanan: spinoam.getJenisPelayanan(),
      nama_perusahaan: spinoam.getNamaPerusahaan(),
      alamat: spinoam.getAlamat(),
      no_sk: spinoam.getNoSk(),
      tgl_exp_sk: spinoam.getTglExpSk(),
      no_uji: spinoam.getNoUji(),
      tgl_exp_uji: spinoam.getTglExpUji(),
      no_kps: spinoam.getNoKps(),
      tgl_exp_kps: spinoam.getTglExpKps(),
      no_rangka: spinoam.getNoRangka(),
      no_mesin: spinoam.getNoMesin(),
      merek: spinoam.getMerek(),
      tahun: spinoam.getTahun(),
      no_kendaraan: spinoam.getNoKendaraan(),
    }).save();

    return {
      id: insertSpinoam.id,
    };
  };
};
