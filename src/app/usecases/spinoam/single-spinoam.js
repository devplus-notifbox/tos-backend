module.exports = function makeSingleSpinoam({ mSpinoam }) {
  return async function singleSpinoam({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const spinoam = await mSpinoam.findByPk(id, { raw: true });

    if (!spinoam) {
      throw new Error("Cannot find spinoam with id " + id + ".");
    }

    return spinoam;
  };
};
