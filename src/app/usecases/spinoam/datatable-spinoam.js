module.exports = function makeDataTableSpinoam({ datatable, mSpinoam }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mSpinoam.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
