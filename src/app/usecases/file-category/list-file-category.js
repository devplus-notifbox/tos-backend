module.exports = function makeListFileCategory({ mFileCategory }) {
  return async function listFileCategory() {
    const fileCategory = await mFileCategory.findAll({
      attributes: ["id", "slug", "name"],
      order: [["name", "ASC"]],
      raw: true,
    });

    return fileCategory;
  };
};
