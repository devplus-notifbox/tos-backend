const { mFileCategory } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleFileCategory = require("./single-file-category");
const makeDataTableFileCategory = require("./datatable-file-category");
const makeAddFileCategory = require("./add-file-category");
const makeEditFileCategory = require("./edit-file-category");
const makeRemoveFileCategory = require("./remove-file-category");
const makeListFileCategory = require("./list-file-category");

const singleFileCategory = makeSingleFileCategory({ mFileCategory });
const dataTableFileCategory = makeDataTableFileCategory({
  datatable,
  mFileCategory,
});
const addFileCategory = makeAddFileCategory({ mFileCategory });
const editFileCategory = makeEditFileCategory({ mFileCategory });
const removeFileCategory = makeRemoveFileCategory({ mFileCategory });
const listFileCategory = makeListFileCategory({ mFileCategory });

module.exports = Object.freeze({
  singleFileCategory,
  dataTableFileCategory,
  addFileCategory,
  editFileCategory,
  removeFileCategory,
  listFileCategory,
});
