module.exports = function makeSingleFileCategory({ mFileCategory }) {
  return async function singleFileCategory({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const action = await mFileCategory.findByPk(id, { raw: true });

    if (!action) {
      throw new Error("Cannot find action with id " + id + ".");
    }

    return action;
  };
};
