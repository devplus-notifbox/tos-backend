module.exports = function makeRemoveFileCategory({ mFileCategory }) {
  return async function removeFileCategory({ id }) {
    const removeFileCategory = await mFileCategory.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeFileCategory,
    };
  };
};
