const { makeFileCategory } = require("../../entities");

module.exports = function makeEditFileCategory({ mFileCategory }) {
  return async function editFileCategory({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mFileCategory.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find Category with id " + id + ".");
    }

    const fileCategory = await makeFileCategory({
      ...existing,
      ...changes,
    });

    await mFileCategory.update(
      {
        slug: fileCategory.getSlug(),
        name: fileCategory.getName(),
        allowed_extension: fileCategory.getAllowedExtension(),
        max_file_size: fileCategory.getMaxFileSize(),
        active: fileCategory.getActive(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
