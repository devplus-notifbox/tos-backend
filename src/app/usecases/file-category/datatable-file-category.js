module.exports = function makeDataTableFileCategory({
  datatable,
  mFileCategory,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mFileCategory.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
