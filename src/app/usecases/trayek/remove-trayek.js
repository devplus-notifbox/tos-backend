module.exports = function makeRemoveTrayek({ mTrayek }) {
  return async function removeTrayek({ id }) {
    const removeTrayek = await mTrayek.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeTrayek,
    };
  };
};
