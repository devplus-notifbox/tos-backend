const { makeTrayek } = require("../../entities");

module.exports = function makeEditTrayek({ mTrayek }) {
  return async function editTrayek({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mTrayek.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find trayek with id " + id + ".");
    }

    const trayek = await makeTrayek({
      ...existing,
      ...changes,
    });

    await mTrayek.update(
      {
        kode: trayek.getKode(),
        nama: trayek.getNama(),
        deskripsi: trayek.getDeskripsi(),
        aktif: trayek.getAktif(),
        terminal: trayek.getTerminal(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
