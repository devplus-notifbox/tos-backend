const { makeTrayek } = require("../../entities");
const { forEach } = require("lodash");
module.exports = function makeAddTrayekAngkutan({ mAngkutan }) {
  return async function addTrayekAngkutan({ trayek_id, data }) {
    if (!trayek_id) {
      throw new Error("Trayek ID is required");
    }
    if (!data) {
      throw new Error("ID Angkutan required");
    }
    if (data.length == 0) {
      throw new Error("ID Angkutan required");
    }

    await Promise.all([
      forEach(data, async (angkutanId) => {
        await mAngkutan.update(
          {
            trayek_id: trayek_id,
          },
          { where: { id: angkutanId } }
        );
      }),
    ]);
    return {
      status: true,
    };
  };
};
