const { makeTrayek } = require("../../entities");

module.exports = function makeAddTrayek({ mTrayek }) {
  return async function addTrayek({ data }) {
    const trayek = await makeTrayek(data);

    const insertTrayek = await new mTrayek({
      kode: trayek.getKode(),
      nama: trayek.getNama(),
      deskripsi: trayek.getDeskripsi(),
      aktif: trayek.getAktif(),
      terminal: trayek.getTerminal(),
    }).save();

    return {
      id: insertTrayek.id,
    };
  };
};
