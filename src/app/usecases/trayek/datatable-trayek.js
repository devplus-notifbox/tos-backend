const Sequelize = require("sequelize");

module.exports = function makeDataTableTrayek({ datatable, mTrayek }) {
  return async function ({ params, terminal } = {}) {
    var dataTableObj = await datatable(params);
    if (terminal) {
      dataTableObj.where = {
        ...dataTableObj.where,
        [Sequelize.Op.and]: Sequelize.literal(
          `JSON_CONTAINS(terminal, '"` + terminal + `"', '$')`
        ),
      };
    }
    var data = await mTrayek.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
