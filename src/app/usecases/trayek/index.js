const { mTrayek, mAngkutan } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleTrayek = require("./single-trayek");
const makeDataTableTrayek = require("./datatable-trayek");
const makeAddTrayek = require("./add-trayek");
const makeEditTrayek = require("./edit-trayek");
const makeRemoveTrayek = require("./remove-trayek");
const makeAddTrayekAngkutan = require("./add-trayek-angkutan");

const singleTrayek = makeSingleTrayek({ mTrayek });
const dataTableTrayek = makeDataTableTrayek({ datatable, mTrayek });
const addTrayek = makeAddTrayek({ mTrayek });
const editTrayek = makeEditTrayek({ mTrayek });
const removeTrayek = makeRemoveTrayek({ mTrayek });
const addTrayekAngkutan = makeAddTrayekAngkutan({ mAngkutan });

module.exports = Object.freeze({
  singleTrayek,
  dataTableTrayek,
  addTrayek,
  editTrayek,
  removeTrayek,
  addTrayekAngkutan,
});
