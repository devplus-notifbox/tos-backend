module.exports = function makeSingleTrayek({ mTrayek }) {
  return async function singleTrayek({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const trayek = await mTrayek.findByPk(id, { raw: true });

    if (!trayek) {
      throw new Error("Cannot find trayek with id " + id + ".");
    }

    return trayek;
  };
};
