const Sequelize = require("sequelize");
const moment = require("moment");
module.exports = function makeDataTablePenumpangBerangkat({
  datatable,
  mPenumpangBerangkat,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);

    var attrData = {
      ...dataTableObj,
      attributes: [
        "terminal_id",
        "terminal",
        "tanggal",
        [Sequelize.col("tanggal"), "jam"],
        "no_kendaraan",
        "tiba",
        "turun",
        "naik",
        // [Sequelize.fn("SUM", Sequelize.col("tiba")), "total_tiba"],
        // [Sequelize.fn("SUM", Sequelize.col("turun")), "total_turun"],
        // [Sequelize.fn("SUM", Sequelize.col("naik")), "total_naik"],
      ],
      // group: [
      //   "terminal_id",
      //   "terminal",
      //   Sequelize.fn("DATE", Sequelize.col("tanggal")),
      //   "no_kendaraan",
      // ],
      order: [["tanggal", "asc"]],
    };
    if (params.date) {
      const from = params.date[0];
      const to = moment(params.date[1]).add(1, "d").format("YYYY-MM-DD");
      attrData.where = {
        [Sequelize.Op.and]: [
          {
            tanggal: {
              [Sequelize.Op.gte]: from,
            },
          },
          {
            tanggal: {
              [Sequelize.Op.lte]: to,
            },
          },
        ],
      };
    }

    var data = await mPenumpangBerangkat.findAndCountAll(attrData);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
