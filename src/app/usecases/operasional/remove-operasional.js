module.exports = function makeRemoveOperasional({ mOperasional }) {
  return async function removeOperasional({ id }) {
    const removeOperasional = await mOperasional.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeOperasional,
    };
  };
};
