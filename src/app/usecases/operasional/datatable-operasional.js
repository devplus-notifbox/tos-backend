const { map, find } = require("lodash");
const { listJenisKendaraan } = require("../../../config/const");
module.exports = function makeDataTableOperasional({
  datatable,
  mOperasional,
  mZone,
  mAngkutan,
  mTrayek,
  mPob,
  mUser,
}) {
  return async function ({ params, isAdmin = true, zoneId = null } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "nik",
          model: "user",
          field: "nik",
        },
        {
          column: "nama_petugas",
          model: "user",
          field: "name",
        },
        {
          column: "terminal",
          model: "zone",
          field: "name",
        },
        {
          column: "terminal_tujuan",
          model: "zone_tujuan",
          field: "name",
        },
        {
          column: "no_kendaraan",
          model: "angkutan",
        },
        {
          column: "trayek",
          model: "angkutan->trayek",
          field: "nama",
        },
        {
          column: "perusahaan_id",
          model: "angkutan->pob",
          field: "kode",
        },
        {
          column: "jenis_kendaraan",
          model: "angkutan",
          field: "jenis_kendaraan",
        },
        {
          column: "perusahaan",
          model: "angkutan->pob",
          field: "nama",
        },
        {
          column: "tipe_angkutan",
          model: "angkutan->trayek",
          field: "tipe",
        },
      ],
    });
    if (!isAdmin && zoneId) {
      dataTableObj.where = {
        ...dataTableObj.where,
        zone_id: zoneId,
      };
    }
    var data = await mOperasional.findAndCountAll({
      ...dataTableObj,
      include: [
        {
          model: mUser,
          attributes: [],
          required: false,
        },
        {
          model: mZone,
          attributes: [],
        },
        {
          model: mZone,
          attributes: [],
          as: "zone_tujuan",
        },
        {
          model: mAngkutan,
          attributes: [],
          required: true,
          include: [
            {
              model: mTrayek,
              attributes: [],
            },
            {
              model: mPob,
              attributes: [],
            },
          ],
        },
      ],
      raw: true,
    });

    data.rows = map(data.rows, (val) => {
      if (val.jenis_kendaraan) {
        val.jenis_kendaraan = find(listJenisKendaraan, {
          id: val.jenis_kendaraan,
        }).nama;
      }

      return val;
    });
    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
