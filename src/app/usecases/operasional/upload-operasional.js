const multer = require("multer");
const path = require("path");
const fs = require("fs");
const config = require("../../../config/app");
var Excel = require("exceljs");
const { makeOperasional } = require("../../entities");
const { Op } = require("sequelize");
const moment = require("moment");
const { forEach } = require("lodash");

module.exports = function makeUploadOperasional({ mOperasional, mAngkutan }) {
  return async function uploadOperasional({ req, res, mimeType }) {
    // const operasional = await makeOperasional(data);
    // console.log(file);
    const uploadPath = "./../../../../public/uploads/operasional";
    const dir = path.join(__dirname + uploadPath);

    const storage = multer.diskStorage({
      destination: dir + "/",
      filename: function (req, file, cb) {
        // console.log(file);
        let ext = null;
        for (var tp in mimeType) {
          if (mimeType[tp] == file.mimetype) {
            ext = tp;
            break;
          }
        }
        const filename = file.originalname;
        var _name = filename.substr(0, filename.lastIndexOf("."));
        _name = _name.replace(/[^a-zA-Z0-9-_\ \.\_\(\)]+/gi, "");
        _name = _name.substring(0, 100);
        newName = _name + "." + ext;

        cb(null, newName);
      },
    });

    const upload = multer({
      storage: storage,
      limits: {
        files: 1,
        fileSize: config.maxFileSize,
      },
      //   fileFilter: (req, file, cb) => {
      //     console.log(file.mimetype);
      //     cb(null, true);
      //     // if (allowedFileType.indexOf(file.mimetype) !== -1) {
      //     //   cb(null, true);
      //     // } else {
      //     //   cb(null, false);
      //     //   return cb(
      //     //     new Error("Only " + allowedFileType.join(", ") + " Allowed")
      //     //   );
      //     // }
      //   },
    }).single("file");

    if (upload) {
      await upload(req, res, async (err) => {
        if (err) {
          throw new Error(err.message);
        }
        const docs = req.file;
        console.log(docs);
        var wb = new Excel.Workbook();
        // var filePath = path.resolve(
        //   __dirname,
        //   uploadPath + "/" + docs.originalname
        // );
        // console.log(filePath);
        wb.xlsx.readFile(docs.path).then(async function () {
          const sh = wb.worksheets[0];
          let y = 1;
          let total = sh.rowCount;
          let errors = [];
          let dataInsert = [];
          for (i = 1; i <= total; i++) {
            if (i > y) {
              try {
                var nik = sh.getRow(i).getCell(1).value;
                var nama_petugas = sh.getRow(i).getCell(2).value;
                var nomor_kendaraan = sh.getRow(i).getCell(3).value;
                var jumlah_penumpang = sh.getRow(i).getCell(4).value;
                var tanggal = sh.getRow(i).getCell(5).value;
                var jam = sh.getRow(i).getCell(6).value;

                if (!tanggal) {
                  throw new Error("Tanggal tidak valid");
                }
                var dateTgl = moment(tanggal);
                if (!dateTgl.isValid()) {
                  throw new Error("Tanggal tidak valid");
                }

                var dateHour = moment(jam);

                if (!dateHour.isValid()) {
                  throw new Error("Jam tidak valid");
                }

                tanggal = dateTgl.format("YYYY-MM-DD");
                jam = dateHour.format("HH:mm");

                var status_spinoam = sh.getRow(i).getCell(7).value;

                status_spinoam = status_spinoam
                  ? status_spinoam.toLowerCase()
                  : "";
                status_spinoam =
                  status_spinoam == "layak"
                    ? true
                    : status_spinoam == "tidak layak"
                    ? false
                    : "";

                var status_eblue = sh.getRow(i).getCell(8).value;
                status_eblue = status_eblue ? status_eblue.toLowerCase() : "";
                status_eblue =
                  status_eblue == "layak"
                    ? true
                    : status_eblue == "tidak layak"
                    ? false
                    : "";
                var note = sh.getRow(i).getCell(9).value;

                console.log(nomor_kendaraan);
                const angkutan = await mAngkutan.findOne({
                  where: {
                    no_kendaraan: {
                      [Op.like]: nomor_kendaraan,
                    },
                  },
                });
                if (!angkutan) {
                  throw new Error(
                    "Kendaraan " + nomor_kendaraan + " tidak terdaftar!"
                  );
                }

                const operasional = await makeOperasional({
                  nik: nik,
                  nama_petugas: nama_petugas,
                  zone_id: req.user.active_zone,
                  tanggal: tanggal,
                  angkutan_id: angkutan.id,
                  jumlah_penumpang: jumlah_penumpang,
                  status_spinoam: status_spinoam,
                  status_eblue: status_eblue,
                  note: note,
                  tipe: req.params.type,
                });

                dataInsert.push({
                  nik: operasional.getNik(),
                  nama_petugas: operasional.getNamaPetugas(),
                  zone_id: operasional.getZoneId(),
                  tanggal: operasional.getTanggal(),
                  angkutan_id: operasional.getAngkutanId(),
                  jumlah_penumpang: operasional.getJumlahPenumpang(),
                  status_spinoam: operasional.getStatusSpinoam(),
                  status_eblue: operasional.getStatusEblue(),
                  note: operasional.getNote(),
                  tipe: operasional.getTipe(),
                });
              } catch (e) {
                console.log(e);
                errors.push("Error Row " + y + ": " + e.message);
              }

              //   const insertOperasional = await new mOperasional({
              //     nik: operasional.getNik(),
              //     nama_petugas: operasional.getNamaPetugas(),
              //     zone_id: operasional.getZoneId(),
              //     tanggal: operasional.getTanggal(),
              //     angkutan_id: operasional.getAngkutanId(),
              //     jumlah_penumpang: operasional.getJumlahPenumpang(),
              //     status_spinoam: operasional.getStatusSpinoam(),
              //     status_eblue: operasional.getStatusEblue(),
              //     note: operasional.getNote(),
              //     tipe: operasional.getTipe(),
              //   }).save();
              y++;
            }
          }

          if (errors.length > 0) {
            res.send({
              error: errors,
            });
            return;
          } else {
            await Promise.all(
              forEach(dataInsert, async (valInsert) => {
                await new mOperasional({ ...valInsert }).save();
              })
            );
            res.send({ success: true });
          }
        });
      });
    }
    return null;
  };
};
