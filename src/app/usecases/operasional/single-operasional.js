module.exports = function makeSingleOperasional({ mOperasional }) {
  return async function singleOperasional({ id } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const operasional = await mOperasional.findByPk(id, { raw: true });

    if (!operasional) {
      throw new Error("Cannot find operasional with id " + id + ".");
    }

    return operasional;
  };
};
