const {
  mOperasional,
  mZone,
  mAngkutan,
  mTrayek,
  mPob,
  mUser,
  mPenumpangBerangkat,
} = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleOperasional = require("./single-operasional");
const makeDataTableOperasional = require("./datatable-operasional");
const makeAddOperasional = require("./add-operasional");
const makeEditOperasional = require("./edit-operasional");
const makeRemoveOperasional = require("./remove-operasional");
const makeUploadOperasional = require("./upload-operasional");
const makeDataTablePenumpangBerangkat = require("./datatable-penumpang-berangkat");

const singleOperasional = makeSingleOperasional({ mOperasional });
const dataTableOperasional = makeDataTableOperasional({
  datatable,
  mOperasional,
  mZone,
  mAngkutan,
  mTrayek,
  mPob,
  mUser,
});
const addOperasional = makeAddOperasional({ mOperasional });
const editOperasional = makeEditOperasional({ mOperasional });
const removeOperasional = makeRemoveOperasional({ mOperasional });
const uploadOperasional = makeUploadOperasional({ mOperasional, mAngkutan });
const dataTablePenumpangBerangkat = makeDataTablePenumpangBerangkat({
  datatable,
  mPenumpangBerangkat,
});

module.exports = Object.freeze({
  singleOperasional,
  dataTableOperasional,
  addOperasional,
  editOperasional,
  removeOperasional,
  uploadOperasional,
  dataTablePenumpangBerangkat,
});
