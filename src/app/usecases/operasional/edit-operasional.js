const { makeOperasional } = require("../../entities");

module.exports = function makeEditOperasional({ mOperasional }) {
  return async function editOperasional({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mOperasional.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find operasional with id " + id + ".");
    }

    const operasional = await makeOperasional({
      ...existing,
      ...changes,
    });

    await mOperasional.update(
      {
        zone_id: operasional.getZoneId(),
        zone_id_tujuan: operasional.getZoneIdTujuan(),
        tanggal: operasional.getTanggal(),
        angkutan_id: operasional.getAngkutanId(),
        jumlah_penumpang: operasional.getJumlahPenumpang(),
        jumlah_penumpang_2: operasional.getJumlahPenumpang2(),
        status_spinoam: operasional.getStatusSpinoam(),
        status_eblue: operasional.getStatusEblue(),
        note: operasional.getNote(),
        tipe: operasional.getTipe(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
