const { makeOperasional } = require("../../entities");

module.exports = function makeAddOperasional({ mOperasional }) {
  return async function addOperasional({ data, userId }) {
    const operasional = await makeOperasional(data);

    const insertOperasional = await new mOperasional({
      user_id: userId,
      zone_id: operasional.getZoneId(),
      zone_id_tujuan: operasional.getZoneIdTujuan(),
      tanggal: operasional.getTanggal(),
      angkutan_id: operasional.getAngkutanId(),
      jumlah_penumpang: operasional.getJumlahPenumpang(),
      jumlah_penumpang_2: operasional.getJumlahPenumpang2(),
      status_spinoam: operasional.getStatusSpinoam(),
      status_eblue: operasional.getStatusEblue(),
      note: operasional.getNote(),
      tipe: operasional.getTipe(),
    }).save();

    return {
      id: insertOperasional.id,
    };
  };
};
