const Sequelize = require("sequelize");
const { trim } = require("lodash");

module.exports = function makeListRegion({
  mVillage,
  mDistrict,
  mCity,
  mProvince,
}) {
  return async function listRegion({ search = null, isId = false }) {
    let result = [];

    if (!search || search.length < 2) {
      return result;
    }

    let whereSearch = null;

    if (isId) {
      whereSearch = {
        id: search,
      };
    } else {
      const arrSearch = search.split(",");
      if (arrSearch.length > 1) {
        if (arrSearch.length == 2) {
          whereSearch = {
            [Sequelize.Op.and]: {
              name: {
                [Sequelize.Op.like]: "%" + trim(arrSearch[0]) + "%",
              },
              ["$district.name$"]: {
                [Sequelize.Op.like]: "%" + trim(arrSearch[1]) + "%",
              },
            },
          };
        } else if (arrSearch.length == 3) {
          whereSearch = {
            [Sequelize.Op.and]: {
              name: {
                [Sequelize.Op.like]: "%" + trim(arrSearch[0]) + "%",
              },
              ["$district.name$"]: {
                [Sequelize.Op.like]: "%" + trim(arrSearch[1]) + "%",
              },
              ["$district.city.name$"]: {
                [Sequelize.Op.like]: "%" + trim(arrSearch[2]) + "%",
              },
            },
          };
        }
      } else {
        whereSearch = {
          [Sequelize.Op.or]: {
            name: {
              [Sequelize.Op.like]: "%" + search + "%",
            },
            ["$district.name$"]: {
              [Sequelize.Op.like]: "%" + search + "%",
            },
            ["$district.city.name$"]: {
              [Sequelize.Op.like]: "%" + search + "%",
            },
          },
        };
      }
    }

    const data = await mVillage.findAll({
      limit: 10,
      raw: true,
      attributes: [
        "id",
        "name",
        [Sequelize.col("district.name"), "district_name"],
        [Sequelize.col("district.city.name"), "city_name"],
        [Sequelize.col("district.city.province.name"), "province_name"],
      ],
      where: {
        ...whereSearch,
      },
      include: [
        {
          model: mDistrict,
          attributes: [],
          required: true,
          include: [
            {
              model: mCity,
              attributes: [],
              required: true,
              include: [
                {
                  model: mProvince,
                  attributes: [],
                  required: true,
                },
              ],
            },
          ],
        },
      ],
    });

    result = data.map((vv) => {
      vv.village_name = vv.name;
      vv.name = vv.name + ", " + vv.district_name + ", " + vv.city_name;

      return vv;
    });

    return result;
  };
};
