const { mVillage, mDistrict, mCity, mProvince } = require("../../models");

const makeListRegion = require("./list-region");

const listRegion = makeListRegion({ mVillage, mDistrict, mCity, mProvince });

module.exports = Object.freeze({
  listRegion,
});
