const { makeUserRole } = require("../../entities");

module.exports = function makeAddUserRole({ mUserRole }) {
  return async function addUserRole({ data }) {
    const userRole = await makeUserRole(data);

    const insertUserRole = await new mUserRole({
      user_id: userRole.getUserId(),
      zone_id: userRole.getZoneId(),
      roles: userRole.getRoles(),
    }).save();

    return {
      id: insertUserRole.id,
    };
  };
};
