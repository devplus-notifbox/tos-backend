const _ = require("lodash");

module.exports = function makeDataTableUserRole({
  datatable,
  mUserRole,
  mZone,
  mUser,
  mRole,
}) {
  return async function ({ params, userId = null } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "zone_name",
          model: "zone",
          field: "name",
        },
        {
          column: "user_name",
          model: "user",
          field: "name",
        },
      ],
    });
    if (userId) {
      dataTableObj.where = _.merge(dataTableObj.where, {
        user_id: userId,
      });
    }
    var data = await mUserRole.findAndCountAll({
      ...dataTableObj,
      include: [
        {
          model: mZone,
          attributes: [],
        },
        {
          model: mUser,
          attributes: [],
        },
      ],
    });

    let listRole = await mRole.findAll({
      attributes: ["slug", "name"],
    });

    data.rows = _.map(data.rows, ({ dataValues }) => {
      if (dataValues.roles) {
        let roleName = [];
        _.forEach(dataValues.roles, (role) => {
          const objRole = _.find(listRole, (o) => o.slug == role);
          if (objRole) {
            roleName.push(objRole.name);
          }
        });
        dataValues.roles_name = roleName;
      }

      return dataValues;
    });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
