const { makeUserRole } = require("../../entities");

module.exports = function makeEditUserRole({ mUserRole }) {
  return async function editUserRole({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mUserRole.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find user role with id " + id + ".");
    }

    const userRole = await makeUserRole({
      ...existing,
      ...changes,
    });

    await mUserRole.update(
      {
        user_id: userRole.getUserId(),
        zone_id: userRole.getZoneId(),
        roles: userRole.getRoles(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
