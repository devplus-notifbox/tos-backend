const { mUserRole, mZone, mUser, mRole } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleUserRole = require("./single-user-role");
const makeDataTableUserRole = require("./datatable-user-role");
const makeAddUserRole = require("./add-user-role");
const makeEditUserRole = require("./edit-user-role");
const makeRemoveUserRole = require("./remove-user-role");

const singleUserRole = makeSingleUserRole({ mUserRole });
const dataTableUserRole = makeDataTableUserRole({
  datatable,
  mUserRole,
  mZone,
  mUser,
  mRole,
});
const addUserRole = makeAddUserRole({ mUserRole });
const editUserRole = makeEditUserRole({ mUserRole });
const removeUserRole = makeRemoveUserRole({ mUserRole });

module.exports = Object.freeze({
  singleUserRole,
  dataTableUserRole,
  addUserRole,
  editUserRole,
  removeUserRole,
});
