module.exports = function makeRemoveUserRole({ mUserRole }) {
  return async function removeUserRole({ id }) {
    const removeUserRole = await mUserRole.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeUserRole,
    };
  };
};
