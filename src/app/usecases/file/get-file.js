const { Op } = require("sequelize");
const { forEach } = require("lodash");
const config = require("../../../config/app");

module.exports = function makeGetFile({ mFile }) {
  return async function getFile({ fileIds } = {}) {
    if (!fileIds) {
      return null;
    }

    const files = await mFile.findAll({
      attributes: [
        "id",
        "name",
        "file_category_id",
        "type",
        "size",
        "extension",
        "path",
      ],
      where: {
        id: {
          [Op.in]: fileIds,
        },
      },
    });

    let data = [];
    forEach(files, (value, key) => {
      data.push({
        id: value.id,
        name: value.name,
        type: value.type,
        size: value.size,
        url: config.fileUrl + value.path,
      });
    });

    return data;
  };
};
