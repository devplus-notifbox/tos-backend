module.exports = function makeDataTableFile({
  datatable,
  mFile,
  mFileCategory,
}) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params, {
      onColumn: [
        {
          column: "category_slug",
          model: "file_category",
          field: "slug",
        },
        {
          column: "category_name",
          model: "file_category",
          field: "name",
        },
      ],
    });
    var data = await mFile.findAndCountAll({
      ...dataTableObj,
      include: [
        {
          model: mFileCategory,
          attributes: [],
        },
      ],
    });

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
