module.exports = function makeRemoveModule({ mModule }) {
  return async function removeModule({ id }) {
    const removeModule = await mModule.destroy({
      where: {
        id,
      },
    });

    return {
      count: removeModule,
    };
  };
};
