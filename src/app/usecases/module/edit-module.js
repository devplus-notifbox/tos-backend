const { makeModule } = require("../../entities");

module.exports = function makeEditModule({ mModule }) {
  return async function editModule({ id, ...changes } = {}) {
    if (!id) {
      throw new Error("You must supply an id.");
    }

    const existing = await mModule.findByPk(id, { raw: true });

    if (!existing) {
      throw new Error("Cannot find module with id " + id + ".");
    }

    const module = await makeModule({
      ...existing,
      ...changes,
    });

    await mModule.update(
      {
        slug: module.getSlug(),
        name: module.getName(),
      },
      { where: { id: id } }
    );

    return {
      id: id,
    };
  };
};
