const { makeModule } = require("../../entities");

module.exports = function makeAddModule({ mModule }) {
  return async function addModule({ data }) {
    const module = await makeModule(data);

    const insertModule = await new mModule({
      slug: module.getSlug(),
      name: module.getName(),
    }).save();

    return {
      id: insertModule.id,
    };
  };
};
