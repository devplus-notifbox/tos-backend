const { mModule } = require("../../models");

const datatable = require("../../../utils/datatable");

const makeSingleModule = require("./single-module");
const makeDataTableModule = require("./datatable-module");
const makeAddModule = require("./add-module");
const makeEditModule = require("./edit-module");
const makeRemoveModule = require("./remove-module");
const makeListModule = require("./list-module");

const singleModule = makeSingleModule({ mModule });
const dataTableModule = makeDataTableModule({ datatable, mModule });
const addModule = makeAddModule({ mModule });
const editModule = makeEditModule({ mModule });
const removeModule = makeRemoveModule({ mModule });
const listModule = makeListModule({ mModule });

module.exports = Object.freeze({
  singleModule,
  dataTableModule,
  addModule,
  editModule,
  removeModule,
  listModule,
});
