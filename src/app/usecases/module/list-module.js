module.exports = function makeListModule({ mModule }) {
  return async function listModule() {
    const module = await mModule.findAll({
      attributes: ["id", "slug", "name"],
      order: [["name", "ASC"]],
      raw: true,
    });

    return module;
  };
};
