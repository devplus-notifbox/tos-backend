module.exports = function makeDataTableModule({ datatable, mModule }) {
  return async function ({ params } = {}) {
    var dataTableObj = await datatable(params);
    var data = await mModule.findAndCountAll(dataTableObj);

    return {
      recordsTotal: data.count,
      data: data.rows,
    };
  };
};
