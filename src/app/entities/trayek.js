const { mTrayek } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeTrayek({ validator }) {
  return async function makeTrayek({
    kode = null,
    nama = null,
    deskripsi = null,
    aktif = null,
    terminal = null,
  } = {}) {
    if (!kode) {
      throw new Error("Kode trayek harus diisi!");
    }
    if (!nama) {
      throw new Error("Nama trayek harus diisi!");
    }
    if (!tipe) {
      throw new Error("Tipe trayek harus diisi!");
    }

    aktif = aktif == null ? false : aktif;

    if (!validator.isBoolean(aktif.toString())) {
      throw new Error("Invalid boolean value for aktif");
    }

    return Object.freeze({
      getKode: () => kode,
      getNama: () => nama,
      getDeskripsi: () => deskripsi,
      getAktif: () => aktif,
      getTerminal: () => terminal,
    });
  };
};
