const { mAngkutan, mPob, mTrayek } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeAngkutan({ validator }) {
  return async function makeAngkutan({
    id = null,
    trayek_id = null,
    pob_id = null,
    tipe = null,
    masa_berlaku = null,
    no_kendaraan = null,
    no_uji = null,
    tgl_kadaluarsa_uji = null,
    no_kps = null,
    tgl_kadaluarsa_kps = null,
    no_srut = null,
    no_rangka = null,
    no_mesin = null,
    merek = null,
    jenis_kendaraan = null,
    tahun_buat = null,
    kapasitas = null,
    deskripsi = null,
    aktif = null,
  } = {}) {
    if (!no_kendaraan) {
      throw new Error("No. Kendaraan harus diisi");
    }

    if (pob_id) {
      const pob = await mPob.findByPk(pob_id);
      if (!pob) {
        throw new Error("PO Bus tidak ditemukan!");
      }
    }

    if (trayek_id) {
      const trayek = await mTrayek.findByPk(trayek_id);
      if (!trayek) {
        throw new Error("Trayek tidak ditemukan!");
      }
    }

    if (!tipe) {
      tipe = "akap";
    }

    if (["akap", "akdp", "pariwisata"].indexOf(tipe) == -1) {
      throw new Error("Jenis angkutan tidak diketahui");
    }

    const _where = {
      no_kendaraan: {
        [Op.like]: no_kendaraan,
      },
    };
    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const noKendaraanExists = await mAngkutan.findOne({
      where: _where,
    });

    if (noKendaraanExists) {
      throw new Error("No. kendaraan " + no_kendaraan + " sudah terdaftar");
    }

    aktif = aktif == null ? false : aktif;

    if (!validator.isBoolean(aktif.toString())) {
      throw new Error("Invalid boolean value for aktif");
    }

    return Object.freeze({
      getTrayekId: () => trayek_id,
      getPobId: () => pob_id,
      getTipe: () => tipe,
      getMasaBerlaku: () => masa_berlaku,
      getNoKendaraan: () => no_kendaraan,
      getNoUji: () => no_uji,
      getTglKadaluarsaUji: () => tgl_kadaluarsa_uji,
      getNoKps: () => no_kps,
      getTglKadaluarsaKps: () => tgl_kadaluarsa_kps,
      getNoSrut: () => no_srut,
      getNoRangka: () => no_rangka,
      getNoMesin: () => no_mesin,
      getMerek: () => merek,
      getJenisKendaraan: () => jenis_kendaraan,
      getTahunBuat: () => tahun_buat,
      getKapasitas: () => kapasitas,
      getDeskripsi: () => deskripsi,
      getAktif: () => aktif,
    });
  };
};
