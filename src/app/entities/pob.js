const { mPob, mVillage } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakePob({ validator }) {
  return async function makePob({
    id = null,
    kode = null,
    nama = null,
    nama_direktur = null,
    no_izin = null,
    tgl_kadaluarsa_izin = null,
    jumlah_kendaraan = null,
    telepon = null,
    kode_pos = null,
    village_id = null,
    alamat = null,
    tipe = null,
    aktif = true,
  } = {}) {
    if (!nama) {
      throw new Error("Nama PO harus diisi");
    }

    if (village_id) {
      const village = await mVillage.findByPk(village_id);
      if (!village) {
        throw new Error("Kelurahan tidak ditemukan!");
      }
    }

    if (kode) {
      const _where = {
        kode: {
          [Op.like]: kode,
        },
      };
      if (id) {
        _where.id = {
          [Op.ne]: id,
        };
      }
      const kodeExists = await mPob.findOne({
        where: _where,
      });

      if (kodeExists) {
        throw new Error("Kode " + kode + " sudah terdaftar");
      }
    }

    aktif = aktif == null ? false : aktif;

    if (!validator.isBoolean(aktif.toString())) {
      throw new Error("Invalid boolean value for aktif");
    }

    return Object.freeze({
      getId: () => id,
      getKode: () => kode,
      getNama: () => nama,
      getNamaDirektur: () => nama_direktur,
      getNoIzin: () => no_izin,
      getTglKadaluarsaIzin: () => tgl_kadaluarsa_izin,
      getJumlahKendaraan: () => jumlah_kendaraan,
      getTelepon: () => telepon,
      getKodePos: () => kode_pos,
      getVillageId: () => village_id,
      getAlamat: () => alamat,
      getTipe: () => tipe,
      getAktif: () => aktif,
    });
  };
};
