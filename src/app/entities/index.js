const validator = require("validator");
const buildMakeModule = require("./module");
const buildMakeAction = require("./action");
const buildMakeZone = require("./zone");
const buildMakeRole = require("./role");
const buildMakeUser = require("./user");
const buildMakeUserRole = require("./user-role");
const buildMakeLogin = require("./login");
const buildMakeFileCategory = require("./file-category");
const buildMakeSdm = require("./sdm");
const buildMakePob = require("./pob");
const buildMakeAngkutan = require("./angkutan");
const buildMakeTerminal = require("./terminal");
const buildMakeTrayek = require("./trayek");
const buildMakeFasilitas = require("./fasilitas");
const buildMakeOperasional = require("./operasional");
const buildMakeDipass = require("./dipass");
const buildMakeEblue = require("./eblue");
const buildMakeSpinoam = require("./spinoam");

const makeModule = buildMakeModule({ validator });
const makeAction = buildMakeAction({ validator });
const makeZone = buildMakeZone({ validator });
const makeRole = buildMakeRole({ validator });
const makeUser = buildMakeUser({ validator });
const makeUserRole = buildMakeUserRole({ validator });
const makeLogin = buildMakeLogin({ validator });
const makeFileCategory = buildMakeFileCategory({ validator });
const makeSdm = buildMakeSdm({ validator });
const makePob = buildMakePob({ validator });
const makeAngkutan = buildMakeAngkutan({ validator });
const makeTerminal = buildMakeTerminal({ validator });
const makeTrayek = buildMakeTrayek({ validator });
const makeFasilitas = buildMakeFasilitas({ validator });
const makeOperasional = buildMakeOperasional({ validator });
const makeDipass = buildMakeDipass({ validator });
const makeEblue = buildMakeEblue({ validator });
const makeSpinoam = buildMakeSpinoam({ validator });

module.exports = Object.freeze({
  makeModule,
  makeAction,
  makeZone,
  makeRole,
  makeUser,
  makeUserRole,
  makeLogin,
  makeFileCategory,
  makeSdm,
  makePob,
  makeAngkutan,
  makeTerminal,
  makeTrayek,
  makeFasilitas,
  makeOperasional,
  makeDipass,
  makeEblue,
  makeSpinoam,
});
