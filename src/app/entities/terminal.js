const { mTerminal, mZone } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeTerminal({ validator }) {
  return async function makeTerminal({
    id = null,
    zone_id = null,
    deskripsi = null,
    gambar = null,
    luas_lahan = null,
    luas_bangunan = null,
    luas_area_pengembangan = null,
    tipe = null,
    status_p3d = null,
    pob = null,
    sk_penetapan_lokasi = null,
    bast = null,
    sertifikat_tanah = null,
  } = {}) {
    if (zone_id) {
      const zone = await mZone.findByPk(zone_id);
      if (!zone) {
        throw new Error("Wilayah tidak ditemukan!");
      }
    }

    return Object.freeze({
      getId: () => id,
      getZoneId: () => zone_id,
      getDeskripsi: () => deskripsi,
      getGambar: () => gambar,
      getLuasLahan: () => luas_lahan,
      getLuasBangunan: () => luas_bangunan,
      getLuasAreaPengembangan: () => luas_area_pengembangan,
      getTipe: () => tipe,
      getStatusP3d: () => status_p3d,
      getPob: () => pob,
      getSkPenetapanLokasi: () => sk_penetapan_lokasi,
      getBast: () => bast,
      getSertifikatTanah: () => sertifikat_tanah,
    });
  };
};
