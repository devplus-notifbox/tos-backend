const { mOperasional, mZone, mAngkutan } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeOperasional({ validator }) {
  return async function makeOperasional({
    id = null,
    user_id = null,
    zone_id = null,
    zone_id_tujuan = null,
    tanggal = null,
    angkutan_id = null,
    jumlah_penumpang = 0,
    jumlah_penumpang_2 = 0,
    status_spinoam = false,
    status_eblue = false,
    note = "",
    tipe = null,
  } = {}) {
    if (zone_id) {
      const zone = await mZone.findByPk(zone_id, {
        where: {
          level: "terminal",
        },
      });
      if (!zone) {
        throw new Error("Terminal tidak ditemukan!");
      }
    }

    if (["in", "out"].indexOf(tipe) == -1) {
      if (!validator.isInt(jumlah_penumpang.toString())) {
        throw new Error("Jumlah penumpang tidak valid");
      }
    } else {
      // jumlah_penumpang = null;
    }

    if (!angkutan_id) {
      throw new Error("No. Kendaraan harus diisi");
    }

    const angkutan = await mAngkutan.findByPk(angkutan_id);

    if (!angkutan) {
      throw new Error("Kendaraan belum terdaftar!");
    }

    if (
      ["in", "out", "tiba", "turun", "naik", "berangkat"].indexOf(tipe) == -1
    ) {
      throw new Error("Tipe tidak valid");
    }

    if (!status_spinoam) {
      status_spinoam = false;
    }
    if (!status_eblue) {
      status_eblue = false;
    }
    if (!validator.isBoolean(status_spinoam.toString())) {
      throw new Error("Invalid boolean value for status spinoam");
    }

    if (!validator.isBoolean(status_eblue.toString())) {
      throw new Error("Invalid boolean value for status eblue");
    }

    return Object.freeze({
      getUserId: () => user_id,
      getZoneId: () => zone_id,
      getZoneIdTujuan: () => zone_id_tujuan,
      getTanggal: () => tanggal,
      getAngkutanId: () => angkutan_id,
      getJumlahPenumpang: () => jumlah_penumpang,
      getJumlahPenumpang2: () => jumlah_penumpang_2,
      getStatusSpinoam: () => status_spinoam,
      getStatusEblue: () => status_eblue,
      getNote: () => note,
      getTipe: () => tipe,
    });
  };
};
