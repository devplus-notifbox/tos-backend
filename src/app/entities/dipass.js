const { mDipass } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeDipass({ validator }) {
  return async function makeDipass({
    id,
    nama = null,
    nama_bus = null,
    no_kursi = null,
    waktu_berangkat = null,
    waktu_tiba = null,
    no_identitas = null,
    rute_asal = null,
    rute_tujuan = null,
  } = {}) {
    if (!nama) {
      throw new Error("Nama harus diisi");
    }

    if (!nama_bus) {
      throw new Error("Nama bus harus diisi");
    }

    if (!no_kursi) {
      throw new Error("Nama bus harus diisi");
    }

    return Object.freeze({
      getNama: () => nama,
      getNamaBus: () => nama_bus,
      getNoKursi: () => no_kursi,
      getWaktuBerangkat: () => waktu_berangkat,
      getWaktuTiba: () => waktu_tiba,
      getNoIdentitas: () => no_identitas,
      getRuteAsal: () => rute_asal,
      getRuteTujuan: () => rute_tujuan,
    });
  };
};
