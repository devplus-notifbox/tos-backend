const { mUserRole, mZone, mUser } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeUserRole({ validator }) {
  return async function makeUserRole({
    id,
    user_id = null,
    zone_id = null,
    roles = null,
  } = {}) {
    if (!user_id) {
      throw new Error("User is required ");
    }
    if (!zone_id) {
      throw new Error("Zone is required");
    }

    // Validate User
    const user = await mUser.findByPk(user_id);
    if (!user) {
      throw new Error("User not exists!");
    }

    // Validate Zone
    const zone = await mZone.findByPk(zone_id);
    if (!zone) {
      throw new Error("Zone not exists!");
    }

    const _where = {
      user_id: user_id,
      zone_id: zone_id,
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    console.log(_where);
    const userRoleExists = await mUserRole.findOne({
      where: _where,
    });

    if (userRoleExists) {
      throw new Error(zone.name + " is already exists!");
    }

    return Object.freeze({
      getId: () => id,
      getUserId: () => user_id,
      getZoneId: () => zone_id,
      getRoles: () => roles,
    });
  };
};
