const { mFasilitas } = require("../models");
const { Op } = require("sequelize");
const { mZone } = require("../models");

module.exports = function buildMakeFasilitas({ validator }) {
  return async function makeFasilitas({
    id,
    zone_id = null,
    nama = null,
    deskripsi = null,
    kategori = null,
    gambar = null,
    aktif = null,
  } = {}) {
    if (!nama) {
      throw new Error("Nama harus diisi");
    }
    if (!zone_id) {
      throw new Error("Terminal harus diisi");
    }

    const terminal = await mZone.findByPk(zone_id);
    if (!terminal) {
      throw new Error("Terminal tidak ditemukan!");
    }

    aktif = aktif == null ? false : aktif;

    if (!validator.isBoolean(aktif.toString())) {
      throw new Error("Invalid boolean value for aktif");
    }

    return Object.freeze({
      getZoneId: () => zone_id,
      getNama: () => nama,
      getDeskripsi: () => deskripsi,
      getKategori: () => kategori,
      getGambar: () => gambar,
      getAktif: () => aktif,
    });
  };
};
