const { mSpinoam } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeSpinoam({ validator }) {
  return async function makeSpinoam({
    id,
    perusahaan_id = null,
    jenis_pelayanan = null,
    nama_perusahaan = null,
    alamat = null,
    no_sk = null,
    tgl_exp_sk = null,
    no_uji = null,
    tgl_exp_uji = null,
    no_kps = null,
    tgl_exp_kps = null,
    no_rangka = null,
    no_mesin = null,
    merek = null,
    tahun = null,
    no_kendaraan = null,
  } = {}) {
    return Object.freeze({
      getPerusahaanId: () => perusahaan_id,
      getJenisPelayanan: () => jenis_pelayanan,
      getNamaPerusahaan: () => nama_perusahaan,
      getAlamat: () => alamat,
      getNoSk: () => no_sk,
      getTglExpSk: () => tgl_exp_sk,
      getNoUji: () => no_uji,
      getTglExpUji: () => tgl_exp_uji,
      getNoKps: () => no_kps,
      getTglExpKps: () => tgl_exp_kps,
      getNoRangka: () => no_rangka,
      getNoMesin: () => no_mesin,
      getMerek: () => merek,
      getTahun: () => tahun,
      getNoKendaraan: () => no_kendaraan,
    });
  };
};
