const { mFileCategory } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeFileCategory({ validator }) {
  return async function makeFileCategory({
    id,
    slug = null,
    name = null,
    allowed_extension = null,
    max_file_size = null,
    active = true,
  } = {}) {
    if (!slug) {
      throw new Error("Category slug is required ");
    }
    if (!name) {
      throw new Error("Category name is required");
    }

    slug = slug.trim();
    name = name.trim();

    const _where = {
      slug: {
        [Op.like]: slug,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const slugExists = await mFileCategory.findOne({
      where: _where,
    });
    if (slugExists) {
      throw new Error("Slug " + slug + " already exists");
    }

    if (!validator.isSlug(slug)) {
      throw new Error("Invalid slug format");
    }

    if (!validator.isBoolean(active.toString())) {
      throw new Error("Invalid boolean value for column active");
    }

    return Object.freeze({
      getId: () => id,
      getSlug: () => slug,
      getName: () => name,
      getAllowedExtension: () => allowed_extension,
      getMaxFileSize: () => max_file_size,
      getActive: () => active,
    });
  };
};
