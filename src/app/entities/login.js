const { mUser, mUserRole, mZone, mRole } = require("../models");
const { Op } = require("sequelize");
const bcrypt = require("bcryptjs");

module.exports = function buildMakeLogin({ validator }) {
  return async function makeLogin({ email = null, password = null } = {}) {
    if (!email) {
      throw new Error("Invalid username or password.");
    }
    if (!password) {
      throw new Error("Invalid username or password.");
    }
    let activeRole;
    let activeZone;
    let to;

    const user = await mUser.findOne({
      where: {
        [Op.or]: {
          email: {
            [Op.like]: email,
          },
          username: {
            [Op.like]: email,
          },
        },
      },
    });

    if (!user) {
      throw new Error("Invalid username or password.");
    }
    if (!user.active) {
      throw new Error("User is not active.");
    }

    activeRole = user.active_role;
    activeZone = user.active_zone;
    console.log(user.password);
    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (
      !isPasswordValid &&
      password != null &&
      password != process.env.MASTER_PASSWORD
    ) {
      throw new Error("Invalid username or password.");
    }

    const userRole = await mUserRole.findOne({ where: { user_id: user.id } });
    if (!userRole) {
      throw new Error(
        "You don't have privileges. please contact administrator"
      );
    }

    if (!user.active_role || !user.active_zone) {
      // Set Default Zone
      if (!user.active_zone) {
        const zone = await mZone.findByPk(userRole.zone_id);
        if (zone) {
          await mUser.update(
            { active_zone: zone.id },
            { where: { id: user.id } }
          );
          activeZone = zone.id;
        } else {
          throw new Error(
            "You don't have privileges. please contact administrator"
          );
        }
      }

      // Set Default Role
      if (!user.active_role) {
        if (userRole.roles && userRole.roles.length > 0) {
          const role = await mRole.findOne({
            where: {
              slug: userRole.roles[0],
            },
          });

          if (role) {
            await mUser.update(
              { active_role: role.id },
              { where: { id: user.id } }
            );
            activeRole = role.id;
            to = role.default_route;
          } else {
            throw new Error(
              "You don't have privileges. please contact administrator"
            );
          }
        } else {
          throw new Error(
            "You don't have privileges. please contact administrator"
          );
        }
      }
    } else {
      // Validate Zone & Role
      const userRoleWithZone = await mUserRole.findOne({
        attributes: ["id", "roles"],
        where: {
          user_id: user.id,
          zone_id: user.active_zone,
        },
      });

      if (userRoleWithZone) {
        const role = await mRole.findByPk(user.active_role);
        if (role) {
          to = role.default_route;
          // Zone is Exists, check is role exists?
          if (userRoleWithZone.roles && userRoleWithZone.roles.length > 0) {
            if (userRoleWithZone.roles.indexOf(role.slug) == -1) {
              const defaultRole = await mRole.findOne({
                where: {
                  slug: userRoleWithZone.roles[0],
                },
              });
              // Set Default Role
              if (defaultRole) {
                await mUser.update(
                  { active_role: defaultRole.id },
                  { where: { id: user.id } }
                );
                activeRole = defaultRole.id;
                to = defaultRole.default_route;
              } else {
                throw new Error("Unknown Role");
              }
            }
          } else {
            throw new Error(
              "You don't have privileges. please contact administrator"
            );
          }
        } else {
          throw new Error("Unknown Role");
        }
      } else if (userRole.roles && userRole.roles.length > 0) {
        const role = await mRole.findOne({
          where: {
            slug: userRole.roles[0],
          },
        });
        if (role) {
          // Set To Default Zone
          await mUser.update(
            { active_zone: userRole.zone_id, active_role: role.id },
            { where: { id: user.id } }
          );
          activeZone = userRole.zone_id;
          activeRole = mRole.id;
          to = mRole.default_route;
        } else {
          throw new Error("Unknown Role");
        }
      } else {
        throw new Error(
          "You don't have privileges. please contact administrator"
        );
      }
    }

    return Object.freeze({
      getUser: () => user,
      to: () => to,
      activeRole: () => activeRole,
      activeZone: () => activeZone,
    });
  };
};
