const { mModule } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeModule({ validator }) {
  return async function makeModule({ id, slug = null, name = null } = {}) {
    if (!slug) {
      throw new Error("Module slug is required ");
    }
    if (!name) {
      throw new Error("Module name is required");
    }

    slug = slug.trim();
    name = name.trim();

    const _where = {
      slug: {
        [Op.like]: slug,
      },
    };

    if (id) {
      _where.id = {
        [Op.ne]: id,
      };
    }
    const slugExists = await mModule.findOne({
      where: _where,
    });
    if (slugExists) {
      throw new Error("Slug " + slug + " already exists");
    }

    if (!validator.isSlug(slug)) {
      throw new Error("Invalid slug format");
    }

    return Object.freeze({
      getId: () => id,
      getSlug: () => slug,
      getName: () => name,
    });
  };
};
