const { mEblue } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeEblue({ validator }) {
  return async function makeEblue({
    id,
    nama_pemilik = null,
    alamat_pemilik = null,
    no_registrasi_kendaraan = null,
    no_rangka = null,
    no_mesin = null,
    jenis_kendaraan = null,
    merk = null,
    tipe = null,
    keterangan_hasil_uji = null,
    masa_berlaku = null,
    petugas_penguji = null,
    nrp_petugas_penguji = null,
    unit_pelaksana_teknis = null,
  } = {}) {
    if (!nama_pemilik) {
      throw new Error("Nama harus diisi");
    }

    return Object.freeze({
      getNamaPemilik: () => nama_pemilik,
      getAlamatPemilik: () => alamat_pemilik,
      getNoRegistrasiKendaraan: () => no_registrasi_kendaraan,
      getNoRangka: () => no_rangka,
      getNoMesin: () => no_mesin,
      getJenisKendaraan: () => jenis_kendaraan,
      getMerk: () => merk,
      getTipe: () => tipe,
      getKeteranganHasilUji: () => keterangan_hasil_uji,
      getMasaBerlaku: () => masa_berlaku,
      getPetugasPenguji: () => petugas_penguji,
      getNrpPetugasPenguji: () => nrp_petugas_penguji,
      getUnitPelaksanaTeknis: () => unit_pelaksana_teknis,
    });
  };
};
