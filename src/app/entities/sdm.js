const { mSdm, mZone, mVillage } = require("../models");
const { Op } = require("sequelize");
module.exports = function buildMakeSdm({ validator }) {
  return async function makeSdm({
    id,
    zone_id,
    nip,
    nama,
    jabatan,
    tipe,
    alamat,
    kode_pos,
    telepon,
    village_id,
  } = {}) {
    if (!nama) {
      throw new Error("Nama harus diisi!");
    }

    if (zone_id) {
      const zone = await mZone.findByPk(zone_id);
      if (!zone) {
        throw new Error("Wilayah tidak ditemukan!");
      }
    }

    if (nip) {
      const _where = {
        nip: {
          [Op.like]: nip,
        },
      };
      if (id) {
        _where.id = {
          [Op.ne]: id,
        };
      }
      const nipExists = await mSdm.findOne({
        where: _where,
      });

      if (nipExists) {
        throw new Error("NIP " + nip + " sudah terdaftar");
      }
    }

    if (village_id) {
      const village = await mVillage.findByPk(village_id);
      if (!village) {
        throw new Error("Kelurahan tidak ditemukan!");
      }
    }

    return Object.freeze({
      getId: () => id,
      getZoneId: () => zone_id,
      getNip: () => nip,
      getNama: () => nama,
      getJabatan: () => jabatan,
      getTipe: () => tipe,
      getAlamat: () => alamat,
      getKodePos: () => kode_pos,
      getTelepon: () => telepon,
      getVillageId: () => village_id,
    });
  };
};
