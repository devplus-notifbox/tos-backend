"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn("terminal", "sk_penetapan_lokasi", {
        type: Sequelize.JSON,
      }),
      queryInterface.addColumn("terminal", "bast", {
        type: Sequelize.JSON,
      }),
      queryInterface.addColumn("terminal", "sertifikat_tanah", {
        type: Sequelize.JSON,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("terminal", "sk_penetapan_lokasi"),
      queryInterface.removeColumn("terminal", "bast"),
      queryInterface.removeColumn("terminal", "sertifikat_tanah"),
    ]);
  },
};
