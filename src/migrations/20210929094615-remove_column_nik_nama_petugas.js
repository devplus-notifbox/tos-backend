"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("operasional", "nik"),
      queryInterface.removeColumn("operasional", "nama_petugas"),
      queryInterface.addColumn("operasional", "user_id", {
        type: Sequelize.UUID,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("operasional", "user_id"),
      queryInterface.addColumn("operasional", "nik", {
        type: Sequelize.STRING(20),
      }),
      queryInterface.addColumn("operasional", "nama_petugas", {
        type: Sequelize.STRING(100),
      }),
    ]);
  },
};
