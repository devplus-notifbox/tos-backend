"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("zone", "village_id", {
      type: Sequelize.STRING(15),
      references: {
        model: "village",
        key: "id",
      },
      onUpdate: "CASCADE",
      onDelete: "SET NULL",
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("zone", "village_id");
  },
};
