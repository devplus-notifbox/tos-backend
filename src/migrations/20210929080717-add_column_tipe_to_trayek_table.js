"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("trayek", "tipe", {
      type: Sequelize.STRING(20),
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("trayek", "tipe");
  },
};
