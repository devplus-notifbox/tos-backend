"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn("angkutan", "tipe", {
        type: Sequelize.STRING(20),
        dafaultValue: "akap",
      }),
      queryInterface.addColumn("angkutan", "masa_berlaku", {
        type: Sequelize.DATEONLY,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("angkutan", "tipe"),
      queryInterface.removeColumn("angkutan", "masa_berlaku"),
    ]);
  },
};
