"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("user", "nik", {
      type: Sequelize.STRING(50),
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("user", "nik");
  },
};
