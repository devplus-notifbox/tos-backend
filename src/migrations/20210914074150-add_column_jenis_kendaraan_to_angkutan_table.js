"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("angkutan", "jenis_kendaraan", {
      type: Sequelize.STRING(50),
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("angkutan", "jenis_kendaraan");
  },
};
