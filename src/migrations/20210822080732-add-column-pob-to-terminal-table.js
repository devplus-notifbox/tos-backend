"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("terminal", "pob", {
      type: Sequelize.JSON,
    });
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("terminal", "pob");
  },
};
