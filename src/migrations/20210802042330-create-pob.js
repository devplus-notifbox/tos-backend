"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("pob", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      kode: {
        type: Sequelize.STRING(20),
        unique: true,
      },
      nama: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      nama_direktur: {
        type: Sequelize.STRING(150),
      },
      jumlah_kendaraan: {
        type: Sequelize.INTEGER,
      },
      no_izin: {
        type: Sequelize.STRING(100),
      },
      tgl_kadaluarsa_izin: {
        type: Sequelize.DATEONLY,
      },
      telepon: {
        type: Sequelize.STRING(20),
      },
      alamat: {
        type: Sequelize.TEXT,
      },
      kode_pos: {
        type: Sequelize.STRING(20),
      },
      village_id: {
        type: Sequelize.STRING(15),
        references: {
          model: "village",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      tipe: {
        type: Sequelize.STRING(20),
      },
      aktif: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("pob");
  },
};
