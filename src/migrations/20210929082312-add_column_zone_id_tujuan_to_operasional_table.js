"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("operasional", "zone_id_tujuan", {
      type: Sequelize.UUID,
    });
  },
  down: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("operasional", "zone_id_tujuan");
  },
};
