"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("spinoam", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      perusahaan_id: {
        type: Sequelize.STRING(50),
      },
      jenis_pelayanan: {
        type: Sequelize.STRING(100),
      },
      nama_perusahaan: {
        type: Sequelize.STRING(100),
      },
      alamat: {
        type: Sequelize.DATE,
      },
      no_sk: {
        type: Sequelize.DATE,
      },
      tgl_exp_sk: {
        type: Sequelize.DATE,
      },
      no_uji: {
        type: Sequelize.STRING(100),
      },
      tgl_exp_uji: {
        type: Sequelize.DATE,
      },
      no_kps: {
        type: Sequelize.STRING(100),
      },
      tgl_exp_kps: {
        type: Sequelize.DATE,
      },
      no_rangka: {
        type: Sequelize.STRING(100),
      },
      no_mesin: {
        type: Sequelize.STRING(100),
      },
      merek: {
        type: Sequelize.STRING(100),
      },
      tahun: {
        type: Sequelize.INTEGER,
      },
      no_kendaraan: {
        type: Sequelize.STRING(20),
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("spinoam");
  },
};
