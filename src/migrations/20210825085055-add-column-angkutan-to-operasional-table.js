"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("operasional", "no_kendaraan"),
      queryInterface.addColumn("operasional", "angkutan_id", {
        type: Sequelize.UUID,
        references: {
          model: "angkutan",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("operasional", "angkutan_id"),
      queryInterface.addColumn("operasional", "no_kendaraan", {
        type: Sequelize.STRING(20),
      }),
    ]);
  },
};
