"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("eblue", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      nama_pemilik: {
        type: Sequelize.STRING(150),
      },
      alamat_pemilik: {
        type: Sequelize.TEXT,
      },
      no_registrasi_kendaraan: {
        type: Sequelize.STRING(100),
      },
      no_rangka: {
        type: Sequelize.STRING(50),
      },
      no_mesin: {
        type: Sequelize.STRING(50),
      },
      jenis_kendaraan: {
        type: Sequelize.STRING(100),
      },
      merk: {
        type: Sequelize.STRING(50),
      },
      tipe: {
        type: Sequelize.STRING(50),
      },
      keterangan_hasil_uji: {
        type: Sequelize.TEXT,
      },
      masa_berlaku: {
        type: Sequelize.DATE,
      },
      petugas_penguji: {
        type: Sequelize.STRING(150),
      },
      nrp_petugas_penguji: {
        type: Sequelize.STRING(150),
      },
      unit_pelaksana_teknis: {
        type: Sequelize.STRING(150),
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("eblue");
  },
};
