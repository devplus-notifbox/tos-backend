"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.removeColumn("trayek", "tipe");
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn("trayek", "tipe", {
      type: Sequelize.STRING(20),
    });
  },
};
