"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn("operasional", "nik", {
        type: Sequelize.STRING(20),
      }),
      queryInterface.addColumn("operasional", "nama_petugas", {
        type: Sequelize.STRING(100),
      }),
      queryInterface.addColumn("operasional", "status_spinoam", {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }),
      queryInterface.addColumn("operasional", "status_eblue", {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      }),
      queryInterface.addColumn("operasional", "note", {
        type: Sequelize.TEXT,
      }),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("operasional", "nik"),
      queryInterface.removeColumn("operasional", "nama_petugas"),
      queryInterface.removeColumn("operasional", "status_spinoam"),
      queryInterface.removeColumn("operasional", "status_eblue"),
      queryInterface.removeColumn("operasional", "note"),
    ]);
  },
};
