"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("sdm", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      zone_id: {
        type: Sequelize.UUID,
        references: {
          model: "zone",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      nip: {
        type: Sequelize.STRING(20),
      },
      nama: {
        type: Sequelize.STRING(150),
        allowNull: false,
      },
      jabatan: {
        type: Sequelize.STRING(200),
      },
      tipe: {
        type: Sequelize.STRING(50),
      },
      alamat: {
        type: Sequelize.TEXT,
      },
      kode_pos: {
        type: Sequelize.STRING(20),
      },
      telepon: {
        type: Sequelize.STRING(20),
      },
      village_id: {
        type: Sequelize.STRING(15),
        references: {
          model: "village",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("sdm");
  },
};
