"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("angkutan", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      trayek_id: {
        type: Sequelize.UUID,
        references: {
          model: "trayek",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      pob_id: {
        type: Sequelize.UUID,
        references: {
          model: "pob",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      no_kendaraan: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      no_uji: {
        type: Sequelize.STRING(100),
      },
      tgl_kadaluarsa_uji: {
        type: Sequelize.DATEONLY,
      },
      no_kps: {
        type: Sequelize.STRING(100),
      },
      tgl_kadaluarsa_kps: {
        type: Sequelize.DATEONLY,
      },
      no_srut: {
        type: Sequelize.STRING(100),
      },
      no_rangka: {
        type: Sequelize.STRING(100),
      },
      no_mesin: {
        type: Sequelize.STRING(100),
      },
      merek: {
        type: Sequelize.STRING(100),
      },
      tahun_buat: {
        type: Sequelize.INTEGER,
      },
      kapasitas: {
        type: Sequelize.INTEGER,
      },
      deskripsi: {
        type: Sequelize.TEXT,
      },
      aktif: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("angkutan");
  },
};
