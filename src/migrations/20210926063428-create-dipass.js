"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("dipass", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      nama: {
        type: Sequelize.STRING(100),
      },
      nama_bus: {
        type: Sequelize.STRING(100),
      },
      no_kursi: {
        type: Sequelize.STRING(50),
      },
      waktu_berangkat: {
        type: Sequelize.DATE,
      },
      waktu_tiba: {
        type: Sequelize.DATE,
      },
      no_identitas: {
        type: Sequelize.STRING(20),
      },
      rute_asal: {
        type: Sequelize.STRING(100),
      },
      rute_tujuan: {
        type: Sequelize.STRING(100),
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("dipass");
  },
};
