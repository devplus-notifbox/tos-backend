"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("operasional", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      zone_id: {
        type: Sequelize.UUID,
        references: {
          model: "zone",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      tanggal: {
        type: Sequelize.DATE,
      },
      no_kendaraan: {
        type: Sequelize.STRING(20),
      },
      jumlah_penumpang: {
        type: Sequelize.INTEGER,
      },
      tipe: {
        type: Sequelize.STRING(10),
      },
      created_by: {
        type: Sequelize.UUID,
      },
      updated_by: {
        type: Sequelize.UUID,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("operasional");
  },
};
